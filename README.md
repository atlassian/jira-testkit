Jira TestKit
============

The Jira TestKit was created to make it easier for plugin developers to set
up integration tests for Jira. 

*NOTE*: Testkit uses jgitver to manage artifact versions. If you eperience problems importing the project in IntelliJ IDEA,
please navigate to `Settigns -> Build, Execution, Deployment -> Build Tools -> Maven -> Importing` and add `-Djgitver.skip=true`
in `VM options for importer`

Using the TestKit
-----------------

The TestKit has two components: the `jira-testkit-plugin` and the
`jira-testkit-client`. The plugin component is a Jira plugin that should be
installed when running integration tests, and the client component contains the
classes that you will interact with in your test code.

### Install the plugin

Using the [Atlassian Plugin SDK] [amps], simply add a `pluginArtifact` to the
`maven-jira-plugin` configuration. This ensures that the plugin gets installed
during integration testing.

        <plugin>
            <groupId>com.atlassian.maven.plugins</groupId>
            <artifactId>maven-jira-plugin</artifactId>
            <configuration>
                <pluginArtifacts>
                    <pluginArtifact>
                        <groupId>com.atlassian.jira.tests</groupId>
                        <artifactId>jira-testkit-plugin</artifactId>
                        <version>${testkit.version}</version>
                    </pluginArtifact>
                </pluginArtifacts>
            </configuration>
        <plugin>

Also add `jira-testkit-client` to your project dependencies so that the
`*Control` classes are made available to your test code.

        <dependency>
            <groupId>com.atlassian.jira.tests</groupId>
            <artifactId>jira-testkit-client</artifactId>
            <version>${testkit.version}</version>
            <scope>test</scope>
        </dependency>

Remember to use `scope=test` for this dependency.

### Call TestKit classes in your tests

The main entry point is the `Backdoor` class. Here is an example of an
integration test that disabled WebSudo and enabled Subtasks during its setup.

        public class MyPluginTest extends FuncTestCase {
            @Override
            protected void setUpTest() {
                super.setUpTest();
                Backdoor testKit = new Backdoor(new TestKitLocalEnvironmentData());
                testKit.restoreBlankInstance(TimeBombLicence.LICENCE_FOR_TESTING);
                testKit.usersAndGroups().addUser("test-user");
                testKit.websudo().disable();
                testKit.subtask().enable();
            }

            // ...
        }

There's a lot more things that the TestKit can do. Have a look at the code!

Compatibility across Jira versions
----------------------------------

The TestKit version numbering scheme mirrors the Jira version that it is
compatible with. For example, TestKit 5.0 is compatible with Jira 5.0. But
because the TestKit builds on the [official Jira Java API] [jiraapi], TestKit
5.0 is also compatible with any 5.x version of Jira, as per the
[Java API Policy for Jira] [promise]. This means you can use TestKit 5.0 in
your integration tests that run against Jira 5.0, Jira 5.1, and Jira 5.2, with
no additional modifications.


  [amps]: https://developer.atlassian.com/display/DOCS/Atlassian+Plugin+SDK+Documentation
  [jiraapi]: https://developer.atlassian.com/static/javadoc/jira/5.0/reference/packages.html
  [promise]: https://developer.atlassian.com/display/JIRADEV/Java+API+Policy+for+JIRA

## Branches
- master - Jira 10+
- testkit_for_jira_9_13
- testkit_for_jira_8_0
- testkit_for_jira_7_12
- testkit_for_jira_7_11 - Jira 7.7 - 7.11
- testkit_for_jira_7_6 - Jira 7.6
- testkit_for_jira_7_2 - Jira 7.2 - 7.5

# Development
This project requires JDK 8 in order to build without error. Maven builds will appear to succeed on JDK 7, but the logs 
will contain a stacktrace about "unsupported major.minor version 52.0".

Before the first running there is a need to build the plugin. Navigate in the console to parent `jira-testkit` directory
and run the build command:
```
mvn clean install
```

To run integration tests run add `-DintegrationTests` flag. That will setup Jira with testkit and run integration
tests against it.

To run plugin locally with Jira navigate to `jira-testkit-plugin` in console. Then execute the command:
```
mvn jira:run
```
or
```
mvn jira:debug
```
to run in debug mode. By default, debugger is available at port `5005`.

In both cases the instance should be available at `http://localhost:2990/jira` (by default).

To make sure the plugin is working correctly, in the working Jira instance, click settings gear -> 'Manage apps'.
The plugin should be listed on the UI.

If there are any issues, stop the instance (Ctrl + D), navigate to parent `jira-testkit` and run the build command
again:
```
mvn clean install
```

## Releasing ##
Testkit releasing procedure is driven by jgitver. For more details, please visit [jgitver home page].
A quick cheat sheet follows.

### I want to release `master` as a release version, e.g. 8.2.7
1. Checkout master branch
2. Create a tag `git tag -m 8.2.7 -a 8.2.7 && git push origin 8.2.7`
3. Run [build plan]

### I am working on a branch (i.e. `issue/borg-317-rest-v2`) and want to use my changes in other project
1. You don't need to do anything, your changes are automatically built and visible as `8.2.7-issue_borg_317_rest_v2-SNAPSHOT` artifact.

### But I don't want to build a -SNAPSHOT, I need concrete version that won't change
1. If your branch is unstable, so you can't merge it to `master`, but you still need a version that won't be overriden with sequenital builds, please run the `deploy HASH` build plan
2. The resulting version will be `8.2.7-fe3d5ac3-issue_borg_317_rest_v2-SNAPSHOT`, where `fe3d5ac3` is last commit's sha1

### What is `8.2.7-SNAPSHOT` version?
1. This is version build from master branch when there were commits after latest tag.
2. You can freely use it in your project, if you want to stay up-to-date with commits happening on master
3. Remember to update your snapshots, i.e. `mvn install -U`

  [artifactory]: https://hello.atlassian.net/wiki/spaces/RELENG/pages/852378105/HOWTO+-+Get+temporary+write+permission+on+artifactory
  [jgitver home page]: https://github.com/jgitver/jgitver?tab=readme-ov-file#how-it-works
  [build plan]: https://server-gdn-bamboo.internal.atlassian.com/browse/JTK-REL