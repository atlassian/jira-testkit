package com.atlassian.jira.testkit.client;

import org.junit.Test;

import static com.atlassian.jira.testkit.client.LocalEnvDataUtil.environmentData;

public class ApplicationPropertiesControlIT {
    @Test
    public void test_property_get() {
        new ApplicationPropertiesControl(environmentData()).getOption("dummy");
    }

    @Test
    public void test_property_set() {
        new ApplicationPropertiesControl(environmentData()).setString("dummy.key", "dummy.value");
    }

}
