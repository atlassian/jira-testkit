package com.atlassian.jira.testkit.client;

import org.junit.Test;

import static com.atlassian.jira.testkit.client.LocalEnvDataUtil.environmentData;

public class LicenseControlIT {
    @Test
    public void test_license_control() {
        new LicenseControl(environmentData()).switchToPersonalLicense();
    }
}
