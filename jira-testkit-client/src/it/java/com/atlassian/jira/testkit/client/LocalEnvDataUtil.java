package com.atlassian.jira.testkit.client;

import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;

import java.io.IOException;
import java.util.Properties;

public class LocalEnvDataUtil {
    private LocalEnvDataUtil() { }

    public static TestKitLocalEnvironmentData environmentData() {
        try {
            Properties properties = new Properties();
            properties.load(LocalEnvDataUtil.class.getResourceAsStream("/it-test.properties"));
            return new TestKitLocalEnvironmentData(properties, null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
