package com.atlassian.jira.testkit.client;

import org.junit.Test;

import static com.atlassian.jira.testkit.client.LocalEnvDataUtil.environmentData;

public class LogControlIT {
    @Test
    public void test_log_control() {
        LogControl logControl = new LogControl(environmentData());
        logControl.info("Log some info");
        logControl.error("Write an error");
    }
}
