package com.atlassian.jira.testkit.client;

import org.junit.Test;

import javax.ws.rs.WebApplicationException;

import static com.atlassian.jira.testkit.client.LocalEnvDataUtil.environmentData;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class PluginsControlIT {
    @Test
    public void gets_plugins_state() {
        String state = new PluginsControl(environmentData())
                .getPluginState("com.atlassian.jira.tests.jira-testkit-plugin");
        assertThat(state).isEqualTo("ENABLED");
    }

    @Test
    public void throws_exception_for_nonexistent_plugin() {
        assertThatThrownBy(() -> new PluginsControl(environmentData())
                .getPluginState("dummy-plugin-name"))
                .isInstanceOf(WebApplicationException.class);
    }

}
