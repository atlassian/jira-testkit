package com.atlassian.jira.testkit.client;

import org.junit.Test;

import static com.atlassian.jira.testkit.client.LocalEnvDataUtil.environmentData;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WebSudoControlIT {

    @Test
    public void can_enable_and_disable() {
        WebSudoControl webSudoControl = new WebSudoControl(environmentData()).loginAs("admin");
        webSudoControl.enable();
        assertTrue(webSudoControl.isEnabled());
        webSudoControl.disable();
        assertFalse(webSudoControl.isEnabled());
    }
}
