package com.atlassian.jira.testkit.client;

import com.atlassian.jira.testkit.client.oauth2.OAuth2ApplinkResponse;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.UUID;

import static javax.ws.rs.client.Entity.json;

/**
 * @since v6.1
 */
public class ApplicationLinkControl extends BackdoorControl<ApplicationLinkControl> {
    public ApplicationLinkControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    static class ApplinkRequest {
        String name;
        String[] redirects;
        String scope;

        public ApplinkRequest(String name, String[] redirects, String scope) {
            this.name = name;
            this.redirects = redirects;
            this.scope = scope;
        }
    }

    public OAuth2ApplinkResponse addOAuth2ApplicationLink(String scope) {
        OAuth2ApplinkResponse applinkResponse =
                resourceRoot(rootPath).path("rest").path("oauth2").path("latest").path("client")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(Entity.json(new ApplinkRequest(
                                "TestkitOAuth2Applink-" + UUID.randomUUID(), new String[] {"http://ignore.it/"}, scope)),
                                OAuth2ApplinkResponse.class);
        return applinkResponse;
    }

    public String addApplicationLink(String type, String name, String url) throws JSONException {
        JSONObject query = prepareAddApplinkQuery(type, name, url);

        return createResource().request(MediaType.APPLICATION_XML)
                .put(json(query.toString()), String.class);
    }

    public String addApplicationLinkAndReturnId(String type, String name, String url) throws JSONException {
        JSONObject query = prepareAddApplinkQuery(type, name, url);

        String response = createResource().request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .put(json(query.toString()), String.class);

        return parseApplinkIdFromJsonPutResponse(response);
    }

    private String parseApplinkIdFromJsonPutResponse(String jsonPutResponse) throws JSONException {
        JSONObject responseObj = new JSONObject(jsonPutResponse);

        String href = responseObj.getJSONArray("resources-created")
                .getJSONObject(0)
                .getString("href");

        URL resourceUrl;
        try {
            resourceUrl = new URL(href);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
        String path = resourceUrl.getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

    public void deleteLink(String linkId) {
        createResource()
                .path(linkId)
                .queryParam("reciprocate", "false")
                .request()
                .delete();
    }

    public List<ApplicationLink> getApplicationLinks() {
        return createResource()
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(ApplicationLinksResponse.class)
                .getApplicationLinks();
    }

    @Override
    protected WebTarget createResource() {
        WebTarget resource = resourceRoot(rootPath).path("rest").path("applinks").path("2.0").path("applicationlink");
        resource.register(BackdoorLoggingFilter.class);
        resource.register(JsonMediaTypeFilter.class);
        return resource;
    }

    private JSONObject prepareAddApplinkQuery(String type, String name, String url) throws JSONException {
        JSONObject query = new JSONObject();
        query.put("name", name);
        query.put("rpcUrl", url);
        query.put("displayUrl", url);
        query.put("typeId", type);
        return query;
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ApplicationLink {
        private String id;
        private String typeId;
        private String name;
        private String displayUrl;
        private String rpcUrl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDisplayUrl() {
            return displayUrl;
        }

        public void setDisplayUrl(String displayUrl) {
            this.displayUrl = displayUrl;
        }

        public String getRpcUrl() {
            return rpcUrl;
        }

        public void setRpcUrl(String rpcUrl) {
            this.rpcUrl = rpcUrl;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ApplicationLinksResponse {
        private List<ApplicationLink> applicationLinks;

        public List<ApplicationLink> getApplicationLinks() {
            return applicationLinks;
        }

        public void setApplicationLinks(List<ApplicationLink> applicationLinks) {
            this.applicationLinks = applicationLinks;
        }
    }
}
