package com.atlassian.jira.testkit.client;

import com.google.common.collect.Sets;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Set;

/**
 * @since v6.2
 */
public class CurrentUserPreferencesClient extends RestApiClient<CurrentUserPreferencesClient>
{
    private static final String PREFERENCES_PATH = "mypreferences";
    private static final String KEY = "key";
    private Set<Response> responses = Sets.newHashSet();

    public CurrentUserPreferencesClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public Response getPreference(final String key)
    {
        WebTarget webTarget = createResource().path(PREFERENCES_PATH);
        if (key != null)
        {
            webTarget = webTarget.queryParam(KEY, key);
        }
        final Response clientResponse = webTarget.request().get(Response.class);
        responses.add(clientResponse);
        return clientResponse;
    }

    public Response setPreference(final String key, final String value)
    {
        WebTarget webTarget = createResource().path(PREFERENCES_PATH);
        if (key != null)
        {
            webTarget = webTarget.queryParam(KEY, key);
        }
        final Response put = webTarget.request("application/json")
                .put(Entity.json(value), Response.class);
        responses.add(put);
        return put;
    }

    public Response removePreference(final String key)
    {
        WebTarget webTarget = createResource().path(PREFERENCES_PATH);
        if (key != null)
        {
            webTarget = webTarget.queryParam(KEY, key);
        }
        final Response delete = webTarget.request().delete(Response.class);
        responses.add(delete);
        return delete;
    }

    public void close()
    {
        for (Response response : responses)
        {
            response.close();
        }
    }
}
