package com.atlassian.jira.testkit.client;

import com.atlassian.jira.entity.property.EntityPropertyType;

import javax.ws.rs.client.WebTarget;

import static javax.ws.rs.client.Entity.json;

public class EntityPropertyControl extends BackdoorControl<EntityPropertyControl> {

    public EntityPropertyControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public String getProperty(EntityPropertyType type, String entityId, String key)
    {
        return propertyWebTarget(type.getDbEntityName(), entityId).path(key).request().get(String.class);
    }

    public String getProperties(EntityPropertyType type, String entityId, String key)
    {
        return propertyWebTarget(type.getDbEntityName(), entityId).request().get(String.class);
    }

    public void putProperty(EntityPropertyType type, String entityId, String key, String value)
    {
        propertyWebTarget(type.getDbEntityName(), entityId).path(key).request().put(json(value));
    }

    public void deleteProperty(EntityPropertyType type, String entityId, String key)
    {
        propertyWebTarget(type.getDbEntityName(), entityId).path(key).request().delete();
    }

    private WebTarget propertyWebTarget(String entityKey, String entityId) {
        return createResource().path("entityproperties").path(entityKey).path(entityId).path("properties");
    }
}
