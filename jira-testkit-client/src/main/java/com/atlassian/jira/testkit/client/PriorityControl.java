/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client;

import com.atlassian.jira.testkit.beans.Priority;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import java.util.List;

import static java.lang.String.valueOf;
import static javax.ws.rs.client.Entity.json;

/**
 * Some helper methods for Statuses.
 *
 * See <code>com.atlassian.jira.testkit.plugin.PriorityBackdoor</code> in jira-testkit-plugin for backend.
 */
public class PriorityControl extends BackdoorControl<PriorityControl>
{
    private static final GenericType<List<Priority>> LIST_GENERIC_TYPE = new GenericType<List<Priority>>(){};

    public PriorityControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public Priority createPriority(String name, String description, String color, String iconUrl)
    {
        final Priority priority = new Priority();
        priority.setName(name);
        priority.setDescription(description);
        priority.setColor(color);
        priority.setIconUrl(iconUrl);
        
        return createPriorityResource().request().post(json(priority), Priority.class);
    }
    
    public Priority editPriority(long id, String name, String description, String color, String iconUrl)
    {
        final Priority priority = new Priority();
        priority.setId(String.valueOf(id));        
        priority.setName(name);
        priority.setDescription(description);
        priority.setColor(color);
        priority.setIconUrl(iconUrl);
        
        return createPriorityResource().request().put(json(priority), Priority.class);
    }

    public List<Priority> getPriorities()
    {
        return createPriorityResource().request().get(LIST_GENERIC_TYPE);
    }

    public void deletePriority(long id)
    {
        createPriorityResource().path(valueOf(id)).request().delete();
    }
    
    public void setDefaultPriority(long id)
    {
        createPriorityResource().path(valueOf(id)).path("default").request().post(null);
    }
    
    public void movePriorityUp(long id)
    {
        createPriorityResource().path(valueOf(id)).path("up").request().post(null);
    }
    
    public void movePriorityDown(long id)
    {
        createPriorityResource().path(valueOf(id)).path("down").request().post(null);
    }

    private WebTarget createPriorityResource()
    {
        return createResource().path("priority");
    }
}
