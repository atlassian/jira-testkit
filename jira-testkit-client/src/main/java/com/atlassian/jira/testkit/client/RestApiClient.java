/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client;

import com.atlassian.jira.testkit.client.dump.FuncTestTimer;
import com.atlassian.jira.testkit.client.dump.TestInformationKit;
import com.atlassian.jira.testkit.client.jerseyclient.ApacheClientFactoryImpl;
import com.atlassian.jira.testkit.client.jerseyclient.JerseyClientFactory;
import com.atlassian.jira.testkit.client.oauth2.OAuth2ApplinkResponse;
import com.atlassian.jira.testkit.client.oauth2.OAuth2Client;
import com.atlassian.jira.testkit.client.restclient.Errors;
import com.atlassian.jira.testkit.client.restclient.ParsedResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.oauth2.OAuth2ClientSupport;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Abstract base class for REST API clients.
 *
 * @since v5.0
 */
public abstract class RestApiClient<T extends RestApiClient<T>>
{
    /**
     * Logger for this client.
     */
    private static final Logger log = LoggerFactory.getLogger(RestApiClient.class);

    /**
     * The REST plugin version to test.
     */
    public static final String REST_VERSION = "2";

    /**
     * Lazily-instantiated Jersey client in thread local variable.
     */
    private static ThreadLocal<Client> client = ThreadLocal.withInitial(() -> {
        ClientConfig config = new ClientConfig();

        // Enable empty body for PUT and POST, allow body for DELETE
        config.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());

        final JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
        jacksonProvider.setMapper(objectMapper);
        jacksonProvider.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        config.register(jacksonProvider);

        config.register(MultiPartWriter.class);


        final JerseyClientFactory clientFactory = new ApacheClientFactoryImpl(config);
        Client client = clientFactory.create();
        if (log.isDebugEnabled()) {
            client.register(new LoggingFeature());
        }
        client.register(BackdoorLoggingFilter.class);
        client.register(JsonMediaTypeFilter.class);
        return client;
    });

    public static class BackdoorLoggingFilter implements ClientRequestFilter, ClientResponseFilter
    {


        private void logRequest(ClientRequestContext request, int status, long howLong)
        {
            LoggerFactory.getLogger(RestApiClient.class)
                    .info(String.format("Backdoor %-6s in %5dms  %s %d", request.getMethod(), howLong, request.getUri().getPath(), status));
        }

        @Override
        public void filter(ClientRequestContext requestContext) throws IOException {
            FuncTestTimer timer = TestInformationKit.pullTimer("Backdoor Shenanigans");
            requestContext.setProperty(FuncTestTimer.class.getCanonicalName(), timer);
        }

        @Override
        public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
            FuncTestTimer timer = (FuncTestTimer) requestContext.getProperty(FuncTestTimer.class.getCanonicalName());
            logRequest(requestContext, responseContext.getStatus(), timer.end());
        }
    }

    /**
     * Sets the {@code Content-Type} header to "{@value MediaType#APPLICATION_JSON}" if not already set.
     */
    public static class JsonMediaTypeFilter implements ClientRequestFilter
    {
        @Override
        public void filter(ClientRequestContext requestContext) throws IOException {
            if (!requestContext.getHeaders().containsKey("Content-Type")) {
                requestContext.getHeaders().putSingle("Content-Type", MediaType.APPLICATION_JSON);
            }
        }
    }



    /**
     * The JIRA environment data
     */
    private final JIRAEnvironmentData environmentData;

    /**
     * The user to log in as.
     */
    protected String loginAs = "admin";

    protected String loginPassword = loginAs;

    protected String oauth2Token;

    private final Set<Response> responses = Sets.newHashSet();

    /**
     * The version of the REST plugin to test.
     */
    private String version;

    /**
     * Constructs a new RestApiClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    protected RestApiClient(JIRAEnvironmentData environmentData)
    {
        this(environmentData, REST_VERSION);
    }

    /**
     * Constructs a new RestApiClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     * @param version a String containing the version to test against
g     */
    protected RestApiClient(JIRAEnvironmentData environmentData, String version)
    {
        this.environmentData = environmentData;
        this.version = version;
    }

    /**
     * Ensures that this client does not authenticate when making a request.
     *
     * @return this
     */
    @SuppressWarnings ("unchecked")
    public T anonymous()
    {
        loginAs = null;
        loginPassword = null;
        oauth2Token = null;
        return (T) this;
    }

    public T authoriseWithOAuth2(OAuth2ApplinkResponse oAuth2ApplinkResponse, String scope) {
        return authoriseWithOAuth2(oAuth2ApplinkResponse.getClientId(), oAuth2ApplinkResponse.getClientSecret(), scope);
    }

    public T authoriseWithOAuth2(String clientId, String clientSecret, String scope) {
        anonymous();
        oauth2Token = new OAuth2Client(environmentData).exchangeClientCredentialsForToken(clientId, clientSecret, scope);
        return (T) this;
    }

    /**
     * Makes this client authenticate as the given user.
     *
     * @param username a String containing the username
     * @return this
     */
    @SuppressWarnings ("unchecked")
    public T loginAs(String username)
    {
        return loginAs(username, username);
    }

    /**
     * Makes this client authenticate as the given <tt>username</tt> and <tt>password</tt>.
     *
     * @param username a String containing the username
     * @param password a String containing the passoword
     * @return this
     */
    @SuppressWarnings ("unchecked")
    public T loginAs(String username, String password)
    {
        oauth2Token = null;
        loginAs = username;
        loginPassword = password;
        return (T) this;
    }

    @Nonnull
    public JIRAEnvironmentData getEnvironmentData()
    {
        return environmentData;
    }

    /**
     * Creates the resource that corresponds to the root of the REST API.
     *
     * @return a WebTarget for the REST API root
     */
    protected WebTarget createResource() {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("api").path(version);
    }

    protected WebTarget createResource(Client client) {
        return resourceRoot(client, environmentData.getBaseUrl().toExternalForm()).path("rest").path("api").path(version);
    }

    /**
     * Creates the resource that corresponds to the root of the internal REST interface.
     *
     * @return a WebTarget for the internal REST interface root
     */
    protected WebTarget createResourceInternal()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("internal").path("1.0");
    }

    /**
     * Creates the resource that corresponds to the root of the gadget REST interface.
     *
     * @return a WebTarget for the gadget REST interface root
     */
    protected WebTarget createResourceGadget()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("gadget").path("1.0");
    }

    /**
     * Creates a WebTarget for the given URL. The relevant authentication parameters are added to the resource, if
     * applicable.
     *
     * @param url a String containing a URL
     * @return a WebTarget, with optional authentication parameters
     */
    protected WebTarget resourceRoot(String url)
    {
        return resourceRoot(client(), url);
    }

    protected WebTarget resourceRoot(Client client, String url)
    {
        WebTarget target = client.target(url);
        if (loginAs != null) {
            target = target.queryParam("os_authType", "basic")
                    .queryParam("os_username", percentEncode(loginAs))
                    .queryParam("os_password", percentEncode(loginPassword));
        }
        if (oauth2Token != null) {
            target.register(OAuth2ClientSupport.feature(oauth2Token));
        }

        return target;
    }

    /**
     * Returns the Jersey client to use.
     *
     * @return a Client
     */
    protected final Client client()
    {
        return client.get();
    }

    protected <T> ParsedResponse<T> toResponse(RestCall restCall)
    {
        Response clientResponse = registerResponse(restCall.call());
        if (clientResponse.getStatus() == 200)
        {
            final ParsedResponse<T> response = new ParsedResponse(clientResponse.getStatus(), null);
            clientResponse.close();
            return response;
        }
        return errorResponse(clientResponse);
    }

    protected <T> ParsedResponse<T> toResponse(RestCall restCall, Class<T> clazz)
    {
         return toResponse(restCall, new GenericType<>(clazz));
    }

    protected <T> ParsedResponse<T> toResponse(RestCall restCall, GenericType<T> clazz)
    {
        Response clientResponse = registerResponse(restCall.call());
        if (clientResponse.getStatus() < 300)
        {
            T object = null;
            if (clientResponse.hasEntity())
            {
                object = clientResponse.readEntity(clazz);
            }
            final ParsedResponse<T> tResponse = new ParsedResponse<T>(clientResponse.getStatus(), null, object);
            clientResponse.close();
            return tResponse;
        }

        return errorResponse(clientResponse);
    }

    protected <T> ParsedResponse<T> errorResponse(Response clientResponse)
    {
        Errors entity = null;
        if (clientResponse.hasEntity() && APPLICATION_JSON_TYPE.isCompatible(clientResponse.getMediaType()))
        {
            try
            {
                entity = clientResponse.readEntity(Errors.class);
            }
            catch (Exception e) { log.debug("Failed to deserialise Errors from response", e); }
        }

        final ParsedResponse<T> response = new ParsedResponse<>(clientResponse.getStatus(), entity);
        clientResponse.close();
        return response;
    }

    /**
     * Adds the expand query param to the given WebTarget. The name of the attributes to expand must exactly match the
     * name of the enum instances that are passed in.
     *
     * @param resource a WebTarget
     * @param expands an EnumSet containing the attributes to expand
     * @return the input WebTarget, with added expand parameters
     */
    protected WebTarget expanded(WebTarget resource, EnumSet<?> expands)
    {
        if (expands.isEmpty())
        {
            return resource;
        }

        return resource.queryParam("expand", percentEncode(StringUtils.join(expands, ",")));
    }

    protected Response registerResponse(Response response) {
        responses.add(response);
        return response;
    }

    public void cleanUp()
    {
        for(Response response : responses)
        {
            response.close();
        }
        responses.clear();
    }

    /**
     * Constructs an EnumSet from a var-args param.
     *
     * @param cls the Enum class object
     * @param expand the enum instances to expand
     * @param <E> the Enum class
     * @return an EnumSet
     */
    protected static <E extends Enum<E>> EnumSet<E> setOf(Class<E> cls, E... expand)
    {
        return expand.length == 0 ? EnumSet.noneOf(cls) : EnumSet.of(expand[0], expand);
    }

    /**
     * Percent-encode the % when stuffing it into a query param. Otherwise it may not get escaped properly, as per <a
     * href="https://extranet.atlassian.com/x/v4Qlbw">this EAC blog</a>.
     *
     * @param queryParam the query param value
     * @return a String with % replaced by %25
     */
    protected static String percentEncode(String queryParam)
    {
        return queryParam == null ? null : queryParam.replace("%", "%25");
    }

    /**
     * Method interface to use with getResponse.
     */
    @FunctionalInterface
    public interface RestCall {
        Response call();
    }
}
