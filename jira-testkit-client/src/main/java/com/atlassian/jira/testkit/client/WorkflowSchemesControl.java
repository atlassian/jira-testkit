/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client;

import com.atlassian.jira.testkit.beans.Status;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.function.Supplier;

import static javax.ws.rs.client.Entity.json;

/**
 * Used to query workflow schemes during the functional tests.
 *
 * @since v5.1
 */
public class WorkflowSchemesControl extends BackdoorControl<WorkflowSchemesControl>
{
    private static final GenericType<List<WorkflowSchemeData>> LIST_GENERIC_TYPE = new GenericType<List<WorkflowSchemeData>>() {
    };

    public WorkflowSchemesControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public WorkflowSchemeData getWorkflowSchemeByProjectKey(String projectKey)
    {
        final WebTarget workflowSchemeResource = createWorkflowSchemeResource();
        return workflowSchemeResource.queryParam("projectKey", projectKey).request().get(WorkflowSchemeData.class);
    }

    public WorkflowSchemeData getWorkflowSchemeByProjectName(String projectName)
    {
        WebTarget workflowSchemeResource = createWorkflowSchemeResource();
        return workflowSchemeResource.queryParam("projectName", projectName).request().get(WorkflowSchemeData.class);
    }

    public WorkflowSchemeData getWorkflowSchemeByName(String schemeName)
    {
        final WebTarget workflowSchemeResource = createWorkflowSchemeResource();
        return workflowSchemeResource.queryParam("schemeName", schemeName).request().get(WorkflowSchemeData.class);
    }

    public WorkflowSchemeData getWorkflowSchemeByNameNullIfNotFound(final String schemeName)
    {
        return nullIfNotFound(() -> getWorkflowSchemeByName(schemeName));
    }

    public WorkflowSchemeData getWorkflowSchemeDraftByProjectNameNullIfNotFound(final String projectName)
    {
        return nullIfNotFound(() -> getWorkflowSchemeDraftByProjectName(projectName));
    }

    public WorkflowSchemeData getWorkflowSchemeDraftByProjectName(String projectName)
    {
        WebTarget workflowSchemeResource = createWorkflowSchemeResource();
        return workflowSchemeResource.queryParam("projectName", projectName).queryParam("draft", "true").request().get(WorkflowSchemeData.class);
    }

    public WorkflowSchemeData getWorkflowSchemeDraftByProjectKey(String projectKey)
    {
        WebTarget workflowSchemeResource = createWorkflowSchemeResource();
        return workflowSchemeResource.queryParam("projectKey", projectKey).queryParam("draft", "true").request().get(WorkflowSchemeData.class);
    }

    public List<WorkflowSchemeData> getWorkflowSchemes()
    {
        return createWorkflowSchemeResource().request().get(LIST_GENERIC_TYPE);
    }

    public WorkflowSchemeData getWorkflowScheme(long id)
    {
        return createWorkflowSchemeResource(id).request().get(WorkflowSchemeData.class);
    }

    public WorkflowSchemeData getWorkflowSchemeForParent(long id)
    {
        return createDraftWorkflowSchemeResource(id).request().get(WorkflowSchemeData.class);
    }

    public WorkflowSchemeData getWorkflowSchemeForParentNullIfNotFound(final long id)
    {
        return nullIfNotFound(() -> getWorkflowSchemeForParent(id));
    }

    public WorkflowSchemeData createDraft(WorkflowSchemeData scheme)
    {
        final WebTarget workflowSchemeResource = createWorkflowSchemeResource(scheme.getId());
        return workflowSchemeResource.path("draft").request().put(json(scheme.getId()), WorkflowSchemeData.class);
    }

    public WorkflowSchemeData createScheme(WorkflowSchemeData scheme)
    {
        final WebTarget workflowSchemeResource = createWorkflowSchemeResource();
        return workflowSchemeResource.request().put(json(scheme), WorkflowSchemeData.class);
    }

    public WorkflowSchemeData updateScheme(WorkflowSchemeData scheme)
    {
        final WebTarget workflowSchemeResource = createWorkflowSchemeResource(scheme.getId());
        return workflowSchemeResource.request().put(json(scheme), WorkflowSchemeData.class);
    }

    public void deleteScheme(long id)
    {
        final WebTarget workflowSchemeResource = createWorkflowSchemeResource(id);
        workflowSchemeResource.request().delete(String.class);
    }

    public WorkflowSchemeData createDraftScheme(long parentId)
    {
        final WebTarget workflowSchemeResource = createDraftWorkflowSchemeResource(parentId);
        return workflowSchemeResource.request().put(json(parentId), WorkflowSchemeData.class);
    }

    public WorkflowSchemeData updateDraftScheme(long parentId, WorkflowSchemeData data)
    {
        final WebTarget workflowSchemeResource = createDraftWorkflowSchemeResource(parentId);
        return workflowSchemeResource.request().post(json(data), WorkflowSchemeData.class);
    }

    public void discardDraftScheme(long parentId)
    {
        final WebTarget workflowSchemeResource = createDraftWorkflowSchemeResource(parentId);
        workflowSchemeResource.request().delete(String.class);
    }

    public Long copyScheme(String schemeName, String newSchemeName)
    {
        return Long.parseLong(createWorkflowSchemeResource().path("copy")
                .queryParam("schemeName", schemeName).queryParam("newSchemeName", newSchemeName).request().get(String.class));
    }

    public void assignScheme(final long schemeId, final String issueType, final String workflowName) {
        final WorkflowSchemeData scheme = getWorkflowScheme(schemeId).setMapping(issueType, workflowName);
        updateScheme(scheme);
    }

    private WebTarget createWorkflowSchemeResource(long id)
    {
        return createWorkflowSchemeResource().path(String.valueOf(id));
    }

    private WebTarget createDraftWorkflowSchemeResource(long id)
    {
        return createWorkflowSchemeResource(id).path("draft");
    }

    private WebTarget createWorkflowSchemeResource()
    {
        return createResource().path("workflowscheme");
    }

    private WorkflowSchemeData nullIfNotFound(Supplier<WorkflowSchemeData> supplier)
    {
        try
        {
            return supplier.get();
        }
        catch (WebApplicationException e)
        {
            if (Response.Status.NOT_FOUND.getStatusCode() == e.getResponse().getStatus())
            {
                return null;
            }

            throw e;
        }
    }
}
