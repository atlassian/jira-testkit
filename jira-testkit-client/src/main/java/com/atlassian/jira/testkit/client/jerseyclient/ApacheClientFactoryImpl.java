/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.jerseyclient;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.glassfish.jersey.apache.connector.ApacheHttpClientBuilderConfigurator;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 * Factory for Jersey clients that use Apache HttpClient.
 *
 * @since v4.3
 */
public class ApacheClientFactoryImpl implements JerseyClientFactory
{
    /**
     * The configuration used for creating the Jersey client.
     */
    private final ClientConfig config;

    /**
     * Creates a ClientFactory with the default configuration, which uses Jackson as the JSON marshaller.
     */
    public ApacheClientFactoryImpl()
    {
        this(new ClientConfig());
        config.getClasses().add(JacksonJsonProvider.class);
    }

    /**
     * Creates a ClientFactory with the provided configuration.
     *
     * @param config a ClientConfig
     */
    public ApacheClientFactoryImpl(ClientConfig config)
    {
        this.config = config;
    }

    /**
     * Creates a Jersey client.
     *
     * @return a new Client instance
     */
    @Override
    public Client create()
    {
        config.register((ApacheHttpClientBuilderConfigurator) httpClientBuilder -> {

            PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
            connectionManager.setDefaultMaxPerRoute(20);
            connectionManager.setMaxTotal(100);

            return httpClientBuilder.setConnectionManager(connectionManager);
        });

        return ClientBuilder.newClient(config);
    }
}
