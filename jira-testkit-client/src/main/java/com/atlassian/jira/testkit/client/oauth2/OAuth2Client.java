package com.atlassian.jira.testkit.client.oauth2;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.nimbusds.oauth2.sdk.AccessTokenResponse;
import com.nimbusds.oauth2.sdk.ClientCredentialsGrant;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenErrorResponse;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.oauth2.sdk.auth.ClientSecretPost;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.AccessToken;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class OAuth2Client {
    private final JIRAEnvironmentData environmentData;

    public OAuth2Client(JIRAEnvironmentData environmentData) {
        this.environmentData = environmentData;
    }

    public String exchangeClientCredentialsForToken(String clientId, String clientSecret, String scope) {
        try {
            TokenRequest request = new TokenRequest(
                    new URI(environmentData.getBaseUrl().toExternalForm() +
                            "/rest/oauth2/latest/token"),
                    new ClientSecretPost(new ClientID(clientId), new Secret(clientSecret)),
                    new ClientCredentialsGrant(), new Scope(scope));

            TokenResponse response = TokenResponse.parse(request.toHTTPRequest().send());
            if (!response.indicatesSuccess()) {
                TokenErrorResponse errorResponse = response.toErrorResponse();
                throw new RuntimeException("Error getting token: " + errorResponse.toString());
            }

            AccessTokenResponse successResponse = response.toSuccessResponse();
            AccessToken accessToken = successResponse.getTokens().getAccessToken();

            return accessToken.toString();
        } catch (URISyntaxException | ParseException | IOException e) {
            throw new RuntimeException("Error getting token", e);
        }

    }
}
