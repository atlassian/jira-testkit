/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Client for the Analytics resource.
 */
public class AnalyticsClient extends RestApiClient<AnalyticsClient>
{
    public enum ReportMode
    {
        BTF("btf_processed"),
        CLOUD("ondemand_processed"),
        RAW("unprocessed");

        final String alias;

        ReportMode(final String alias)
        {
            this.alias = alias;
        }
    }

    /**
     * Constructs a new AttachmentClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public AnalyticsClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public void acknowledgePolicy()
    {
        toResponse(() -> createResource().path("config").path("acknowledge")
                        .request(APPLICATION_JSON_TYPE)
                        .put(null, Response.class));
    }

    public void disable() {
        toResponse(() ->  createResource().path("config").path("enable")
                        .request(APPLICATION_JSON_TYPE)
                        .put(json(new AnalyticsEnabled(false)), Response.class));
    }


    public void enable() {
        toResponse(() -> createResource().path("config").path("enable")
                        .request(APPLICATION_JSON_TYPE)
                        .put(json(new AnalyticsEnabled(true)), Response.class));
    }

    /**
     * Start capturing events for getReport()
     */
    public void startCapturing()
    {
        toResponse(() -> createResource().path("report")
                        .request(APPLICATION_JSON_TYPE)
                        .put(json(new AnalyticsReportConfig(true)), Response.class));
    }

    /**
     * Stop capturing events for getReport()
     */
    public void stopCapturing()
    {
        toResponse(() -> createResource().path("report")
                        .request(APPLICATION_JSON_TYPE)
                        .put(json(new AnalyticsReportConfig(false)), Response.class));
    }

    /**
     * Deletes all captured events in getReport()
     */
    public void clearCaptured()
    {
        toResponse(() -> createResource().path("report").request().delete(Response.class));
    }

    /**
     * Get the report of all events raised between startCapturing() and stopCapturing()
     */
    public ParsedResponse getReport(final ReportMode reportMode)
    {
        return toResponse(() -> createResource().path("report").queryParam("mode", reportMode.alias)
                .request().get(Response.class));
    }



    @Override
    protected WebTarget createResource()
    {
        return resourceRoot(getEnvironmentData().getBaseUrl().toExternalForm()).path("rest").path("analytics").path("1.0");
    }
}
