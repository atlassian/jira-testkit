package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnalyticsReportConfig
{
    public Boolean capturing;

    public AnalyticsReportConfig() {}

    public AnalyticsReportConfig(final Boolean capturing)
    {
        this.capturing = capturing;
    }
}

