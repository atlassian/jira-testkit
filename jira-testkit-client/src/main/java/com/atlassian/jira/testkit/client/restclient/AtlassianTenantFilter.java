/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;

/**
 * Adds the X-Atlassian-Tenant header to all requests
 *
 * @since v4.3
 */
public class AtlassianTenantFilter implements ClientRequestFilter
{
    private final String tenant;

    public AtlassianTenantFilter(String tenant)
    {
        this.tenant = tenant;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        if(!requestContext.getHeaders().containsKey("X-Atlassian-Tenant")) {
            requestContext.getHeaders().add("X-Atlassian-Tenant", tenant);
        }
    }
}
