/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Client for the Attachment resource.
 *
 * @since v4.3
 */
public class AttachmentClient extends RestApiClient<AttachmentClient>
{
    /**
     * Constructs a new AttachmentClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public AttachmentClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the attachment with the given id.
     *
     * @param attachmentID a String containing the attachment id
     * @return an Attachment
     * @throws WebApplicationException if there is a problem
     */
    public Attachment get(String attachmentID) throws WebApplicationException
    {
        return attachmentWithID(attachmentID)
                .request().get(Attachment.class);
    }

    /**
     * GETs the attachment with the given ID, and returns the Response.
     *
     * @param attachmentID a String containing the attachment ID
     * @return a Response
     */
    public ParsedResponse getResponse(final String attachmentID)
    {
        return toResponse(() -> attachmentWithID(attachmentID).request().get(Response.class));
    }

    /**
     * Deletes the attachment with the given ID, and returns the Response.
     *
     * @param attachmentID a String containing the attachment ID
     * @return a Response
     */
    public ParsedResponse deleteResponse(final String attachmentID)
    {
        return toResponse(() -> attachmentWithID(attachmentID).request().delete(Response.class));
    }

    /**
     * Returns the WebTarget for the attachment having the given id.
     *
     * @param attachmentID a String containing the attachment id
     * @return a WebTarget
     */
    protected WebTarget attachmentWithID(String attachmentID)
    {
        return createResource().path("attachment").path(attachmentID);
    }

    /**
     * GETs the global attachment meta
     *
     * @return a Response
     */
    public Map getMeta()
    {
        return attachmentMeta().request().get(Map.class);
    }

    /**
     * Returns the WebTarget for the attachment meta
     *
     * @return a WebTarget
     */
    protected WebTarget attachmentMeta()
    {
        return createResource().path("attachment").path("meta");
    }

}
