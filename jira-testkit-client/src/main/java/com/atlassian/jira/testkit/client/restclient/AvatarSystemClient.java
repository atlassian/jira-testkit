package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Client for the Attachment resource.
 *
 * @since v8.1.34
 */
public class AvatarSystemClient extends RestApiClient<AvatarSystemClient> {
    private static final String AVATARS_SYSTEM = "avatar/%s/system";

    public AvatarSystemClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public Map<String, List<Avatar>> getSystemAvatars(String type) {
        return createResource().path(String.format(AVATARS_SYSTEM, type)).request().get(Avatar.ALLAVATARS_TYPE);
    }

    public Response getSystemAvatarsResponse(String type) {
        final WebTarget webTarget = createResource().path(String.format(AVATARS_SYSTEM, type));
        return registerResponse(webTarget.request().get(Response.class));
    }
}

