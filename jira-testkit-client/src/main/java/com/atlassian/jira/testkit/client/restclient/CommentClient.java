/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

import static javax.ws.rs.client.Entity.json;

/**
 * Client for the Comment resource.
 *
 * @since v4.3
 */
public class CommentClient extends RestApiClient<CommentClient>
{
    /**
     * Constructs a new CommentClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public CommentClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the comment with the given ID.
     *
     * @param issueKey the comment belongs to
     * @param commentID a String containing a comment id
     * @return a Comment
     * @throws WebApplicationException if there is a problem getting the comment
     */
    public ParsedResponse<Comment> get(final String issueKey, final String commentID) throws WebApplicationException
    {
        return toResponse(() ->  commentWithID(issueKey, commentID).request().get(Response.class), Comment.class);
    }

    public ParsedResponse<Comment> get(final String issueKey, final String commentID, final String expand) throws WebApplicationException
    {
        return toResponse(() ->  commentWithID(issueKey, commentID)
                        .queryParam("expand", expand)
                        .request()
                        .get(Response.class), Comment.class);
    }

    public ParsedResponse<CommentsWithPaginationBean> getComments(final String issueKey)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("comment")
                .request().get(Response.class), CommentsWithPaginationBean.class);
    }

    public ParsedResponse<CommentsWithPaginationBean> getComments(final String issueKey, final Integer startAt, final Integer maxResults, final String orderBy)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("comment")
                        .queryParam("startAt", Objects.toString(startAt))
                        .queryParam("maxResults", Objects.toString(maxResults))
                        .queryParam("orderBy", orderBy)
                        .request()
                        .get(Response.class), CommentsWithPaginationBean.class);
    }

    public ParsedResponse<CommentsWithPaginationBean> getComments(final String issueKey, final String expand)
    {
        return toResponse(() ->  createResource()
                        .path("issue").path(issueKey).path("comment")
                        .queryParam("expand", expand)
                        .request()
                        .get(Response.class), CommentsWithPaginationBean.class);
    }

    public ParsedResponse<Comment> put(final String issueKey, final Comment comment)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("comment").path(comment.id)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(json(comment), Response.class), Comment.class);
    }

    public ParsedResponse<Comment> put(final String issueKey, final Comment comment, final String expand)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("comment").path(comment.id)
                        .queryParam("expand", expand)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(json(comment), Response.class), Comment.class);
    }

    public ParsedResponse<Comment> post(final String issueKey, final Comment comment)
    {
        return toResponse(() ->  createResource()
                        .path("issue").path(issueKey).path("comment")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(comment), Response.class), Comment.class);
    }

    public ParsedResponse<Comment> post(final String issueKey, final Comment comment, final String expand)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("comment")
                        .queryParam("expand", expand)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(comment), Response.class), Comment.class);
    }

    public ParsedResponse delete(final String issueKey, final Comment comment)
    {
        return delete(issueKey, comment.id);
    }

    public ParsedResponse delete(final String issueKey, final String commentId)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("comment").path(commentId)
                        .request(MediaType.APPLICATION_JSON_TYPE).delete(Response.class));
    }

    /**
     * GETs the comment with the given ID, and returns a Response.
     *
     * @param issueKey the comment belongs to
     * @param commentID a String containing a comment ID
     * @return a Response
     */
    public ParsedResponse getResponse(final String issueKey, final String commentID)
    {
        return toResponse(() ->  commentWithID(issueKey, commentID).request().get(Response.class));
    }

    /**
     * Returns a WebTarget for the comment with the given ID.
     *
     * @param issueKey the comment belongs to
     * @param commentID a String containing a comment ID
     * @return a WebTarget
     */
    protected WebTarget commentWithID(String issueKey, String commentID)
    {
        return createResource().path("issue").path(issueKey).path("comment").path(commentID);
    }
}
