/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Set;

import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Client for the component resource.
 *
 * @since v4.3
 */
public class ComponentClient extends RestApiClient<ComponentClient> {
    /**
     * Constructs a new ComponentClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public ComponentClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * GETs the component with the given ID.
     *
     * @param componentID a String containing a component ID
     * @return a Component
     * @throws WebApplicationException if anything goes wrong
     */
    public Component get(String componentID) throws WebApplicationException {
        return componentWithId(componentID).request().get(Component.class);
    }

    /**
     * GETs the component with the given ID, and returns the Response.
     *
     * @param componentID a String containing the component ID
     * @return a Response
     */
    public ParsedResponse getResponse(final String componentID) {
        return toResponse(() -> componentWithId(componentID).request().get(Response.class));
    }

    /**
     * Returns the web resource for a given comment.
     *
     * @param componentID a String containing the comment ID
     * @return a WebTarget
     */
    protected WebTarget componentWithId(String componentID) {
        return createResource().path("component").path(componentID);
    }

    public Component create(Component component) {
        try {
            return component().post(json(component), Component.class);
        } catch (WebApplicationException e) {
            throw new RuntimeException("Failed to create component: " + errorResponse(e.getResponse()), e);
        }
    }

    public ParsedResponse createResponse(final Component component) {
        return toResponse(() -> component().post(json(component), Response.class));
    }

    public ParsedResponse putResponse(final String componentId, final Component component) {
        return toResponse(() -> componentWithId(componentId).request(APPLICATION_JSON_TYPE)
                .put(json(component), Response.class));
    }

    public ParsedResponse putResponse(final Component component) {
        final String[] selfParts = component.self.split("/");
        final String componentId = selfParts[selfParts.length - 1];
        return putResponse(componentId, component);
    }

    public ComponentIssueCounts getComponentIssueCounts(String componentId) throws WebApplicationException {
        return componentWithId(componentId).path("relatedIssueCounts").request().get(ComponentIssueCounts.class);
    }

    public ParsedResponse getComponentIssueCountsResponse(final String componentId) {
        return toResponse(() -> componentWithId(componentId).path("relatedIssueCounts").request().get(Response.class));
    }

    public ParsedResponse delete(final String componentId) throws WebApplicationException {
        return delete(componentId, null);
    }

    public ParsedResponse delete(final String componentId, final URI swapComponent) throws WebApplicationException {
        return toResponse(() -> {
            WebTarget deleteResource = componentWithId(componentId);
            if (swapComponent != null) {
                deleteResource = deleteResource.queryParam("moveIssuesTo", swapComponent.getPath());
            }
            return deleteResource.request().delete(Response.class);
        });
    }

    /**
     * Returns paginated list of filtered components
     *
     * @param startAt    the index of the first component to return
     * @param maxResults the maximum number of component to return
     * @param query      the string that component names will be matched with
     * @param projectIds the set of project ids by which components will be filtered
     */
    public PageBean<Component> getPage(final Long startAt, final Integer maxResults, final String query, final Set<String> projectIds) {
        WebTarget webTarget = this.createResource()
                .path("component")
                .path("page")
                .queryParam("startAt", startAt.toString())
                .queryParam("maxResults", maxResults.toString())
                .queryParam("query", query);

        for (String projectId : projectIds) {
            webTarget = webTarget.queryParam("projectIds", projectId);
        }
        return webTarget.request().get(new GenericType<PageBean<Component>>() {
        });
    }

    /**
     * Returns a WebResponse for the component resource
     *
     * @return a WebTarget
     */
    private Invocation.Builder component() {
        return createResource().path("component").request(MediaType.APPLICATION_JSON_TYPE);
    }
}
