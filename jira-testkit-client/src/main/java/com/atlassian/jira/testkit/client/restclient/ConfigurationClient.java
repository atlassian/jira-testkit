package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.core.Response;

public class ConfigurationClient extends RestApiClient<ConfigurationClient>
{
    public ConfigurationClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public ConfigurationBean getConfiguration()
    {
        return createResource().path("configuration").request().get(ConfigurationBean.class);
    }

    public ParsedResponse<ConfigurationBean> getConfigurationResponse()
    {
        return toResponse(() ->  createResource().path("configuration").request().get(Response.class),
                ConfigurationBean.class);
    }
}