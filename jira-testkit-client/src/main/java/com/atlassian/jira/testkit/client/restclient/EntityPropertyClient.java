package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.util.json.JSONObject;
import io.atlassian.fugue.Option;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Client for entity property resource.
 *
 * @since v6.2
 */
public class EntityPropertyClient extends RestApiClient<EntityPropertyClient>
{
    protected final String entityName;

    /**
     * Constructs an entity property client for JIRA instance.
     *
     * @param environmentData the JIRA environmental data
     * @param entityName the property name
     */
    public EntityPropertyClient(final JIRAEnvironmentData environmentData, String entityName)
    {
        super(environmentData);
        this.entityName = entityName;
    }

    /**
     * Gets the properties keys for the entity with given id or key.
     *
     * @param entityKeyOrId key or id of an entity.
     * @return list of entity properties keys.
     */
    public EntityPropertyKeys getKeys(String entityKeyOrId)
    {
        return resource(entityKeyOrId).request().get(EntityPropertyKeys.class);
    }

    /**
     * Returns the property for the given entity and property key.
     *
     * @param entitykeyOrId key or id of an entity.
     * @param propertyKey key of the property to return.
     * @return see above
     */
    public EntityProperty get(String entitykeyOrId, String propertyKey)
    {
        return resource(entitykeyOrId, propertyKey).request().get(EntityProperty.class);
    }

    /**
     * Sets the value of the property with given key, associated with a given entity.
     *
     * @param entityKeyOrId key or id of an entity.
     * @param propertyKey key of the property.
     * @param value value of the property.
     */
    public ParsedResponse<String> put(final String entityKeyOrId, final String propertyKey, final JSONObject value)
    {
        return toResponse(() -> resource(entityKeyOrId, propertyKey).request(APPLICATION_JSON)
                .put(Entity.json(value.toString())), String.class);
    }

    /**
     * Removes the value of the property with given key, associated with a given entity.
     *
     * @param entityKeyOrId key or id of an entity.
     * @param propertyKey key of the property to remove.
     */
    public ParsedResponse delete(final String entityKeyOrId, final String propertyKey)
    {
        return toResponse(() -> resource(entityKeyOrId, propertyKey).request().delete());
    }

    public WebTarget resource(String entityKeyOrId)
    {
        return resource(Option.<String>some(entityKeyOrId), Option.<String>none());
    }

    public WebTarget resource(String entityKeyOrId, String propertyKey)
    {
        return resource(Option.<String>some(entityKeyOrId), Option.some(propertyKey));
    }

    protected WebTarget resource(final Option<String> entityKeyOrId, final Option<String> propertyKey)
    {
        WebTarget webTarget = createResource().path(entityName);

        if ("user".equals(entityName)) {
            webTarget = webTarget.queryParam("username", entityKeyOrId.getOrElse(""));
        } else {
            webTarget = webTarget.path(entityKeyOrId.getOrElse(""));
        }

        return webTarget.path("properties").path(propertyKey.getOrElse(""));
    }
}
