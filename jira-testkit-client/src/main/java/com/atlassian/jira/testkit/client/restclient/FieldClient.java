/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.rest.api.customfield.CustomFieldDefinitionJsonBean;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.client.Entity.json;

/**
 * Client for the field resource.
 *
 * @since v5.0
 */
public class FieldClient extends RestApiClient<FieldClient> {
    /**
     * Constructs a new FieldClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public FieldClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * GETs all fieldes
     *
     * @return a Field
     * @throws WebApplicationException if there's a problem getting the field
     */
    public List<Field> get() throws WebApplicationException {
        return field().request().get(new GenericType<List<Field>>() {
        });
    }

    public ParsedResponse getResponse() {
        return toResponse(() -> field().request().get(Response.class));
    }


    public ParsedResponse createCustomFieldResponse(CustomFieldDefinitionJsonBean customFieldDefinitionJson) {
        return toResponse(() ->  field().request(MediaType.APPLICATION_JSON_TYPE).post(json(customFieldDefinitionJson), Response.class));
    }

    /**
     * GETs the field with the given id, returning a Response object.
     *
     * @param fieldID a String containing the field id
     * @return a Response
     */
    public ParsedResponse getResponse(final String fieldID) {
        return toResponse(() ->  fieldWithID(fieldID).request().get(Response.class));
    }

    /**
     * Returns a WebTarget for the all field.
     *
     * @return a WebTarget
     */
    protected WebTarget field() {
        return createResource().path("field");
    }

    /**
     * Returns a WebTarget for the field having the given id.
     *
     * @param fieldID a String containing the field id
     * @return a WebTarget
     */
    protected WebTarget fieldWithID(String fieldID) {
        return createResource().path("field").path(fieldID);
    }
}
