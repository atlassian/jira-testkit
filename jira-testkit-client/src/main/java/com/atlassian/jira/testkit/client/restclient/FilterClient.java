/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import static javax.ws.rs.client.Entity.json;

/**
 * Client for the Filter resource.
 *
 * @since v5.0
 */
public class FilterClient extends RestApiClient<FilterClient>
{
    /**
     * Constructs a new FilterClient for a JIRA instance.
     *
     * @param environmentData the JIRA environment data
     */
    public FilterClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the filter with the given ID.
     *
     * @param filterId a String containing a filter id
     * @param expand the properties to expand
     * @return a Filter
     * @throws WebApplicationException if there is a problem getting the filter
     */
    public Filter get(String filterId, Filter.Expand... expand) throws WebApplicationException
    {
        return filterWithId(filterId, expand).request().get(Filter.class);
    }

    public List<Filter> getFavouriteFilters(Filter.Expand... expand)
    {
        return addExpands(createResource().path("filter").path("favourite"), expand).request().get(Filter.FILTER_TYPE);
    }

    /**
     * GETs the filter with the given ID, and returns a Response.
     *
     * @param filterId a String containing a filter ID
     * @return a Response
     */
    public ParsedResponse getResponse(final String filterId)
    {
        return toResponse(() ->  filterWithId(filterId).request().get(Response.class));
    }

    /**
     * Creates a WebTarget.Builder for the filter resource.
     *
     * @return a WebTarget.Builder
     */
    private WebTarget filterTarget()
    {
        return createResource().path("filter");
    }

    private WebTarget addExpands(WebTarget resource, Filter.Expand... expand)
    {
        EnumSet<Filter.Expand> expands = setOf(Filter.Expand.class, expand);
        if (expands.isEmpty())
        {
            return resource;
        }
        return resource.queryParam("expand", percentEncode(StringUtils.join(expands, ",")));
    }

    /**
     * Returns a WebTarget for the filter with the given ID.
     *
     * @param filterId a String containing a filter ID
     * @param expand any expands
     * @return a WebTarget
     */
    protected WebTarget filterWithId(String filterId, Filter.Expand... expand)
    {
        return addExpands(createResource().path("filter").path(filterId), expand);
    }

    /**
     * Creates a filter using a POST, and returns a Response. The JQL query and other parameters are passed in the JSON
     * payload.
     *
     * @param filter a Filter object
     * @param expand any expands
     * @return a Response
     */
    public ParsedResponse<Filter> postFilterResponse(final Filter filter, final Filter.Expand... expand)
    {
        return toResponse(() ->  filterResourceForPost(expand)
                        .post(json(filter), Response.class), Filter.class);
    }

    /**
     * Updates a filter using a PUT, and returns a Response. The JQL query and other parameters are passed in the JSON
     * payload.
     *
     * @param filter a Filter object
     * @param expand any expands
     * @return a Response
     */
    public ParsedResponse<Filter> putFilterResponse(final Filter filter, final Filter.Expand... expand)
    {
        return toResponse(() ->  filterResourceForPut(filter.id, expand)
                        .put(json(filter), Response.class), Filter.class);
    }


    public ParsedResponse<List<Filter.FilterPermission>> getFilterPermissions(String filterId)
    {
        return toResponse(() ->  filterWithId(filterId).path("permission").request().get(Response.class),
                new GenericType<List<Filter.FilterPermission>>(){});
    }

    public ParsedResponse<Filter.FilterPermission> getFilterPermission(String filterId, String filterPermissionId)
    {
        return toResponse(() ->  filterWithId(filterId).path("permission").path(filterPermissionId).request()
                .get(Response.class), Filter.FilterPermission.class);
    }

    public ParsedResponse deleteFilterPermission(String filterId, String filterPermissionId)
    {
        return toResponse(() ->  filterWithId(filterId).path("permission").path(filterPermissionId).request()
                .delete(Response.class));
    }

    public ParsedResponse<List<Filter.FilterPermission>> postFilterPermission(String filterId, FilterPermissionInputBean inputBean)
    {
        return toResponse(() ->  filterWithId(filterId).path("permission").request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(inputBean), Response.class), new GenericType<List<Filter.FilterPermission>>(){});
    }

    public static class FilterPermissionInputBean
    {
        public String type;
        public String projectId;
        public String projectRoleId;
        public String groupname;
        public String userKey;
        public boolean view;
        public boolean edit;

        public FilterPermissionInputBean(String type, String projectId, String projectRoleId, String groupname) {
            this.type = type;
            this.projectId = projectId;
            this.projectRoleId = projectRoleId;
            this.groupname = groupname;
        }

        public FilterPermissionInputBean(String type, String projectId, String projectRoleId, String groupname, String userKey, boolean view, boolean edit) {
            this(type, projectId, projectRoleId, groupname);
            this.userKey = userKey;
            this.view = view;
            this.edit = edit;
        }
    }

    /**
     * Returns a WebTarget.Builder that can be used to PUT a search.
     *
     * @param filterId the filter ID
     * @param expand   any expands
     * @return see above
     */
    private Invocation.Builder filterResourceForPut(String filterId, Filter.Expand... expand)
    {
        return addExpands(filterTarget().path(filterId), expand).request(MediaType.APPLICATION_JSON_TYPE);
    }


    /**
     * Returns a WebTarget.Builder that can be used to POST a search.
     *
     * @param expand any expands
     * @return a WebTarget.Builder
     */
    private Invocation.Builder filterResourceForPost(Filter.Expand... expand)
    {
        return addExpands(filterTarget(), expand).request(MediaType.APPLICATION_JSON_TYPE);
    }

    public Map<String,String> getDefaultShareScope()
    {
        return createResource().path("filter").path("defaultShareScope").request().get(new GenericType<Map<String, String>>(){});
    }

    public Map<String,String> setDefaultShareScope(Map<String,String> scope)
    {
        return createResource().path("filter").path("defaultShareScope")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(json(scope), new GenericType<Map<String, String>>(){});
    }
}
