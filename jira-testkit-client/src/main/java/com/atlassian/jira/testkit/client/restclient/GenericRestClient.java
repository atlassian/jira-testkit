package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * Rest client to invoke methods on arbitrary URI-s.
 */
public final class GenericRestClient extends RestApiClient<GenericRestClient>
{
    public GenericRestClient()
    {
        super(null);
    }

    public <T> ParsedResponse<T> get(final URI path, Class<T> responseClass)
    {
        return toResponse(() ->  getResource(path), responseClass);
    }

    public <T> ParsedResponse<T> get(final URI path, GenericType<T> responseType)
    {
        return toResponse(() ->  getResource(path), responseType);
    }


    public <T> ParsedResponse<PageBean<T>> getNextPage(PageBean<T> currentPage, GenericType<PageBean<T>> actualPageType)
    {
        if (Boolean.TRUE.equals(currentPage.getIsLast()) || currentPage.getNextPage() == null) {
            throw new IllegalArgumentException("last page or next URL not specified");
        }

        return get(currentPage.getNextPage(), actualPageType);
    }


    private Response getResource(final URI path)
    {
        return client()
                .target(path)
                .queryParam("os_authType", "basic")
                .queryParam("os_username", percentEncode(loginAs))
                .queryParam("os_password", percentEncode(loginPassword))
                .request()
                .get(Response.class);
    }
}
