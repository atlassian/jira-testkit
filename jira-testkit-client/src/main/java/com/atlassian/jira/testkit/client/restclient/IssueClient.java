/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.rest.api.issue.RemoteIssueLinkCreateOrUpdateRequest;
import com.atlassian.jira.rest.api.issue.RemoteIssueLinkCreateOrUpdateResponse;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.util.collect.MapBuilder;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Client for the issue resource.
 *
 * @since v4.3
 */
public class IssueClient extends RestApiClient<IssueClient>
{
    /**
     * Constructs a new IssueClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public IssueClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the issue with the given key.
     *
     * @param issueKey a String containing an issue key
     * @param expand the attributes to expand
     * @return an Issue
     * @throws WebApplicationException if there's a problem getting the issue
     */
    public Issue get(String issueKey, Issue.Expand... expand) throws WebApplicationException
    {
        return get(issueKey, false, expand);
    }

    /**
     * GETs the issue with the given key.
     *
     * @param issueKey a String containing an issue key
     * @param updateHistory if true then issue will be added to the user's history
     * @param expand the attributes to expand
     * @return an Issue
     * @throws WebApplicationException if there's a problem getting the issue
     */
    public Issue get(String issueKey, boolean updateHistory, Issue.Expand... expand) throws WebApplicationException
    {
        return issueResource(issueKey, updateHistory, expand).request().get(Issue.class);
    }

    public Issue getWithProperties(String issueKey, List<String> properties, Issue.Expand... expand) {
        return issueResource(issueKey, false, expand).queryParam("properties", String.join(",", properties)).request().get(Issue.class);
    }

    public WebTarget issueResource(String issueKey, Issue.Expand... expand)
    {
        return issueWithKey(issueKey, Collections.<StringList>emptyList(), setOf(Issue.Expand.class, expand), null);
    }

    public WebTarget issueResource(String issueKey, boolean updateHistory, Issue.Expand... expand)
    {
        return issueWithKey(issueKey, Collections.<StringList>emptyList(), setOf(Issue.Expand.class, expand), updateHistory);
    }

    /**
     * GETs the issue with the given key, returning only the request fields.
     *
     * @param issueKey a String containing an issue key
     * @param fields the list of fields to return for the issue
     * @return an Issue
     * @throws WebApplicationException if there's a problem getting the issue
     */
    public Issue getPartially(String issueKey, StringList... fields) throws WebApplicationException
    {
        return getPartially(issueKey, setOf(Issue.Expand.class), fields);
    }

    public Issue getPartially(String issueKey, EnumSet<Issue.Expand> expand, StringList... fields) throws WebApplicationException
    {
        return issueWithKey(issueKey, Arrays.asList(fields), expand, null).request().get(Issue.class);
    }

    /**
     * GETs the issue from the given URL.
     *
     * @param issueURL a String containing the valid URL for an issue
     * @param expand the attributes to expand
     * @return an Issue
     * @throws WebApplicationException if there's a problem getting the issue
     */
    public Issue getFromURL(String issueURL, Issue.Expand... expand) throws WebApplicationException
    {
        final EnumSet<Issue.Expand> expands = setOf(Issue.Expand.class, expand);
        return expanded(resourceRoot(issueURL), expands).request().get(Issue.class);
    }

    public IssueCreateResponse create(IssueUpdateRequest issue)
    {
        return create(issue, false);
    }

    public IssueCreateResponse create(IssueUpdateRequest issue, boolean updateHistory)
    {
        try
        {
            return createResource()
                    .queryParam("updateHistory", Boolean.toString(updateHistory))
                    .request(APPLICATION_JSON_TYPE)
                    .post(entity(issue, APPLICATION_JSON), IssueCreateResponse.class);
        }
        catch (WebApplicationException e)
        {
            throw new RuntimeException("Failed to create issue: " + errorResponse(e.getResponse()), e);
        }
    }

    public void edit(final String issueKey, final IssueUpdateRequest updateRequest)
    {
        try
        {
            createResourceWithIssueKey(issueKey).request(APPLICATION_JSON_TYPE).put(json(updateRequest));
        }
        catch (WebApplicationException e)
        {
            throw new RuntimeException("Failed to edit issue: " + errorResponse(e.getResponse()), e);
        }
    }

    /**
     * Creates an issue as per the request, and returns the Response.
     *
     * @param issue an IssueCreateRequest
     * @return a Response
     */
    public ParsedResponse getResponse(final IssueUpdateRequest issue)
    {
        return toResponse(() ->  createResource().request(APPLICATION_JSON_TYPE).post(json(issue), Response.class));
    }

    public ParsedResponse operationalUpdateResponse(final String issueKey, final OperationalUpdateRequest update)
    {
        return toResponse(() ->  createResource().path(issueKey).request(APPLICATION_JSON_TYPE)
                        .put(json(update), Response.class));
    }

    public void operationalUpdate(final String issueKey, OperationalUpdateRequest updateRequest)
    {
        try
        {
            createResource().path(issueKey).request(APPLICATION_JSON_TYPE).put(json(updateRequest));
        }
        catch (WebApplicationException e)
        {
            throw new RuntimeException("Failed to update issue: " + errorResponse(e.getResponse()), e);
        }
    }

    public void update(String issueKey, IssueUpdateRequest issue)
    {
        try
        {
            createResource().path(issueKey).request(APPLICATION_JSON_TYPE).put(json(issue), String.class);
        }
        catch (WebApplicationException e)
        {
            throw new RuntimeException("Failed to update issue: " + errorResponse(e.getResponse()), e);
        }
    }

    public ParsedResponse updateResponse(final String issueKey, final IssueUpdateRequest update)
    {
        return toResponse(() ->  createResource().path(issueKey).request(APPLICATION_JSON_TYPE).put(json(update),
                Response.class));
    }

    public ParsedResponse updateResponse(final String issueKey, final IssueUpdateRequest update, final boolean notifyUsers)
    {
        return toResponse(() ->  createResource()
                        .path(issueKey)
                        .queryParam("notifyUsers", Boolean.toString(notifyUsers))
                        .request(APPLICATION_JSON_TYPE)
                        .put(json(update), Response.class));
    }

    public ParsedResponse update(final String issueKey, final Map update)
    {
        return toResponse(() ->  createResource().path(issueKey).request(APPLICATION_JSON_TYPE).put(json(update), Response.class));
    }

    /**
     * DELETEs the issue with the given key.
     *
     * @param issueKey a String containing an issue key or id
     * @param deleteSubtasks the attributes to expand
     * @return a Response
     * @throws WebApplicationException if there's a problem deleting the issue
     */
    public ParsedResponse delete(final String issueKey, final String deleteSubtasks) throws WebApplicationException
    {
        return toResponse(() -> {
            WebTarget deleteResource = createResource().path(issueKey);
            if (deleteSubtasks != null)
            {
                deleteResource = deleteResource.queryParam("deleteSubtasks", deleteSubtasks);
            }
            return deleteResource.request().delete(Response.class);
        });
    }

    /**
     * Assigns an issue to the given user.
     *
     * @param issueKey a String containing an issue key or id
     * @param assignee user object
     * @return a Response
     * @throws WebApplicationException if there's a problem deleting the issue
     */
    public ParsedResponse assign(final String issueKey, final User assignee) throws WebApplicationException
    {
        return toResponse(() -> {
                WebTarget assignResource = createResource().path(issueKey).path("assignee");
                return assignResource.request(APPLICATION_JSON_TYPE).put(json(assignee));
            },
                Response.class);
    }

    /**
     * Transitions an issue along the given transition ID.
     *
     * @param issueKey a String containing an issue key or id
     * @param transition an object containing a transition ID
     * @return
     */
    public ParsedResponse transition(final String issueKey, final IssueUpdateRequest transition)
    {
        return toResponse(() ->
                createResource().path(issueKey).path("transitions").request(APPLICATION_JSON_TYPE).post(json(transition)),
                Response.class);
    }


    /**
     * GETs the issue with the given key, returning a Response.
     *
     * @param issueKey a String containing an issue key
     * @return a Response
     */
    public ParsedResponse getResponse(final String issueKey)
    {
        return toResponse(() ->  issueWithKey(issueKey, null, setOf(Issue.Expand.class), null).request()
                .get(Response.class));
    }

    /**
     * Returns a WebTarget for the issue with the given key.
     *
     * @param issueKey a String containing an issue key
     * @param fields the list of fields to return for the issue
     * @param expand what to expand
     * @param updateHistory if true then issue will be added to the user's history
     * @return a WebTarget
     */
    protected WebTarget issueWithKey(String issueKey, @Nullable List<StringList> fields, EnumSet<Issue.Expand> expand, Boolean updateHistory)
    {
        WebTarget resource = createResource().path(issueKey);
        resource = addStringListsToQueryParams(resource, "fields", fields);
        if( updateHistory!=null ) {
            resource = resource.queryParam("updateHistory", Boolean.toString(updateHistory));
        }

        return expanded(resource, expand);
    }

    /**
     * Returns the meta data for creating issues.
     *
     * @param projectIds the list of projects to filter on
     * @param projectKeys the list of projects to filter on
     * @param issueTypeIds the list of issue types to filter on
     * @param issueTypeNames the issue type names
     * @param expand what to expand
     * @return an IssueCreateMeta
     *
     * @deprecated This endpoint no longer exists since Jira 9.0.
     * @see <a href="https://confluence.atlassian.com/jiracore/createmeta-rest-endpoint-to-be-removed-975040986.html">Createmeta REST endpoint to be removed</a>
     */
    @Deprecated
    public IssueCreateMeta getCreateMeta(@Nullable final List<StringList> projectIds, @Nullable final List<StringList> projectKeys,
            @Nullable final List<StringList> issueTypeIds, @Nullable final List<String> issueTypeNames,
            final IssueCreateMeta.Expand... expand)
    {
        return getCreateMetaResource(projectIds, projectKeys, issueTypeIds, issueTypeNames, setOf(IssueCreateMeta.Expand.class, expand))
                .request().get(IssueCreateMeta.class);
    }

    /**
     * Gets the meta data for creating issues, returning a Response.
     *
     * @param projectIds the list of projects to filter on
     * @param projectKeys the list of projects to filter on
     * @param issueTypeIds the list of issue types to filter on
     * @param issueTypeNames the issue type names
     * @param expand what to expand
     * @return a Response
     *
     * @deprecated This endpoint no longer exists since Jira 9.0.
     * @see <a href="https://confluence.atlassian.com/jiracore/createmeta-rest-endpoint-to-be-removed-975040986.html">Createmeta REST endpoint to be removed</a>
     */
    @Deprecated
    public ParsedResponse getCreateMetaResponse(@Nullable final List<StringList> projectIds, @Nullable final List<StringList> projectKeys,
                                                @Nullable final List<StringList> issueTypeIds, @Nullable final List<String> issueTypeNames,
                                                final IssueCreateMeta.Expand... expand)
    {
        return toResponse(() -> getCreateMetaResource(projectIds, projectKeys, issueTypeIds, issueTypeNames, setOf(IssueCreateMeta.Expand.class, expand))
                        .request().get(Response.class));
    }

    /**
     * Returns WebTarget, containing the meta data for creating issues.
     *
     * @param projectIds the list of projects to filter on
     * @param projectKeys the list of projects to filter on
     * @param issueTypeIds the list of issue types to filter on
     * @param issueTypeNames the issue type names
     * @param expand what to expand
     * @return a WebTarget
     *
     * @deprecated This endpoint no longer exists since Jira 9.0.
     * @see <a href="https://confluence.atlassian.com/jiracore/createmeta-rest-endpoint-to-be-removed-975040986.html">Createmeta REST endpoint to be removed</a>
     */
    @Deprecated
    private WebTarget getCreateMetaResource(@Nullable final List<StringList> projectIds, @Nullable final List<StringList> projectKeys,
            @Nullable final List<StringList> issueTypeIds, @Nullable final List<String> issueTypeNames,
            final EnumSet<IssueCreateMeta.Expand> expand)
    {
        WebTarget resource = createResource().path("createmeta");
        resource = addStringListsToQueryParams(resource, "projectIds", projectIds);
        resource = addStringListsToQueryParams(resource, "projectKeys", projectKeys);
        resource = addStringListsToQueryParams(resource, "issuetypeIds", issueTypeIds);
        resource = addStringsToQueryParams(resource, "issuetypeNames", issueTypeNames);
        resource = expanded(resource, expand);

        return resource;
    }

    /**
     * Returns issue types for creating issues.
     *
     * @param projectIdOrKey project for filtering
     * @param startAt the index of the first issue type to return (0-based)
     * @param maxResults the maximum number of issue types to return in a single request
     * @return a WebTarget
     */
    public PageBean<IssueType> getCreateIssueMetaProjectIssueTypes(@Nonnull final String projectIdOrKey,
                                                               @Nullable final Long startAt,
                                                               @Nullable final Integer maxResults)
    {
        return getCreateIssueMetaProjectIssueTypesResource(projectIdOrKey, startAt, maxResults)
                .request().get(new GenericType<PageBean<IssueType>>(){});
    }

    /**
     * Returns WebTarget, containing the issue types for creating issues.
     *
     * @param projectIdOrKey project for filtering
     * @param startAt the index of the first issue type to return (0-based)
     * @param maxResults the maximum number of issue types to return in a single request
     * @return a WebTarget
     */
    public WebTarget getCreateIssueMetaProjectIssueTypesResource(@Nonnull final String projectIdOrKey,
                                                                    @Nullable final Long startAt,
                                                                    @Nullable final Integer maxResults)
    {
        WebTarget resource = createResource().path("createmeta").path(projectIdOrKey).path("issuetypes");
        resource = resource.queryParam("startAt", startAt == null ? null : startAt.toString());
        resource = resource.queryParam("maxResults", maxResults == null ? null : maxResults.toString());

        return resource;
    }

    /**
     * Returns issue type for creating issues.
     *
     * @param projectIdOrKey project for filtering
     * @param issueTypeId issue type for filtering
     * @param startAt the index of the first issue type to return (0-based)
     * @param maxResults the maximum number of issue types to return in a single request
     * @return a WebTarget
     */
    public PageBean<IssueType> getCreateIssueMetaProjectIssueType(@Nonnull final String projectIdOrKey,
                                                                  @Nonnull final String issueTypeId,
                                                                   @Nullable final Long startAt,
                                                                   @Nullable final Integer maxResults)
    {
        return getCreateIssueMetaProjectIssueTypeResource(projectIdOrKey, issueTypeId, startAt, maxResults)
                .request().get(new GenericType<PageBean<IssueType>>(){});
    }

    /**
     * Returns WebTarget, containing the issue type for creating issues.
     *
     * @param projectIdOrKey project for filtering
     * @param issueTypeId issue type for filtering
     * @param startAt the index of the first issue type to return (0-based)
     * @param maxResults the maximum number of issue types to return in a single request
     * @return a WebTarget
     */
    public WebTarget getCreateIssueMetaProjectIssueTypeResource(@Nonnull final String projectIdOrKey,
                                                                   @Nonnull final String issueTypeId,
                                                                   @Nullable final Long startAt,
                                                                   @Nullable final Integer maxResults)
    {
        WebTarget resource = createResource().path("createmeta").path(projectIdOrKey).path("issuetypes").path(issueTypeId);
        resource = resource.queryParam("startAt", startAt == null ? null : startAt.toString());
        resource = resource.queryParam("maxResults", maxResults == null ? null : maxResults.toString());

        return resource;
    }

    /**
     * Returns WebTarget, containing the fields for creating issues.
     *
     * @param projectIdOrKey project for filtering
     * @param issueTypeId issue type for filtering
     * @param startAt the index of the first issue type to return (0-based)
     * @param maxResults the maximum number of issue types to return in a single request
     * @return a WebTarget
     */
    public PageBean<FieldMetaData> getCreateIssueMetaFields(@Nonnull final String projectIdOrKey,
                                                         @Nonnull final String issueTypeId,
                                                         @Nullable final Long startAt,
                                                         @Nullable final Integer maxResults)
    {
        return getCreateIssueMetaFieldsResource(projectIdOrKey, issueTypeId, startAt, maxResults)
                .request().get(new GenericType<PageBean<FieldMetaData>>(){});
    }

    /**
     * Returns WebTarget, containing the fields for creating issues.
     *
     * @param projectIdOrKey project for filtering
     * @param issueTypeId issue type for filtering
     * @param startAt the index of the first issue type to return (0-based)
     * @param maxResults the maximum number of issue types to return in a single request
     * @return a WebTarget
     */
    private WebTarget getCreateIssueMetaFieldsResource(@Nonnull final String projectIdOrKey,
                                                         @Nonnull final String issueTypeId,
                                                         @Nullable final Long startAt,
                                                         @Nullable final Integer maxResults)
    {
        return getCreateIssueMetaProjectIssueTypesResource(projectIdOrKey, startAt, maxResults).path(issueTypeId);
    }

    /**
     * Create a remote link.
     *
     * @param issueKey the issue key
     * @param remoteIssueLink the remote issue link
     * @return a RemoteIssueLinkCreateOrUpdateResponse
     */
    public RemoteIssueLinkCreateOrUpdateResponse createOrUpdateRemoteIssueLink(
            final String issueKey, final RemoteIssueLinkCreateOrUpdateRequest remoteIssueLink)
    {
        try
        {
            return createResource().path(issueKey).path("remotelink")
                    .request(APPLICATION_JSON_TYPE)
                    .post(json(remoteIssueLink), RemoteIssueLinkCreateOrUpdateResponse.class);
        }
        catch (WebApplicationException e)
        {
            throw new RuntimeException("Failed to create remote link: " + errorResponse(e.getResponse()), e);
        }
    }

    /**
     * Create a remote link, and return a Response. This is useful for checking error conditions.
     *
     * @param issueKey the issue key
     * @param remoteIssueLink the remote issue link
     * @return a Response
     */
    public ParsedResponse createOrUpdateRemoteIssueLinkAndGetResponse(
            final String issueKey, final RemoteIssueLinkCreateOrUpdateRequest remoteIssueLink)
    {
        return toResponse(() ->  createResource().path(issueKey).path("remotelink")
                        .request(APPLICATION_JSON_TYPE)
                        .post(json(remoteIssueLink), Response.class));
    }

    /**
     * Create a remote link, and return a Response.
     *
     * @param issueKey the issue key
     * @param remoteIssueLink the remote issue link
     * @return a Response
     */
    public Response createOrUpdateRemoteIssueLinkAndGetClientResponse(
            final String issueKey, final RemoteIssueLinkCreateOrUpdateRequest remoteIssueLink)
    {
        return createResource().path(issueKey).path("remotelink")
                        .request(APPLICATION_JSON_TYPE)
                        .post(json(remoteIssueLink), Response.class);
    }

    /**
     * Update a remote link, and return a Response. This is useful for checking error conditions.
     *
     * @param issueKey the issue key
     * @param linkId the link ID
     * @param remoteIssueLink the remote issue link
     * @return a Response
     */
    public ParsedResponse updateRemoteIssueLink(
            final String issueKey, final String linkId, final RemoteIssueLinkCreateOrUpdateRequest remoteIssueLink)
    {
        return toResponse(() ->  createResource().path(issueKey).path("remotelink").path(linkId)
                        .request(APPLICATION_JSON_TYPE)
                        .put(json(remoteIssueLink), Response.class));
    }


    /**
     * Delete a remote link, and return a Response.
     *
     * @param issueKey the issue key
     * @param remoteIssueLinkId the remote issue link
     * @return a Response
     */
    public ParsedResponse deleteRemoteIssueLink(final String issueKey, final String remoteIssueLinkId)
    {
        return toResponse(() ->  createResource().path(issueKey).path("remotelink").path(remoteIssueLinkId)
                        .request(APPLICATION_JSON_TYPE)
                        .delete(Response.class));
    }

    /**
     * Delete a remote link by global id, and return a Response.
     *
     * @param issueKey the issue key
     * @param globalId the global ID
     * @return a Response
     */
    public ParsedResponse deleteRemoteIssueLinkByGlobalId(final String issueKey, final String globalId)
    {
        return toResponse(() ->  createResource().path(issueKey).path("remotelink").queryParam("globalId", globalId)
                        .request().delete(Response.class));
    }

    /**
     * Returns a remote link.
     *
     * @param issueKey the issue key
     * @param remoteIssueLinkId the remote issue link ID
     * @return a RemoteIssueLink
     */
    public RemoteIssueLink getRemoteIssueLink(final String issueKey, final String remoteIssueLinkId)
    {
        return getRemoteIssueLinkResource(issueKey, remoteIssueLinkId, null).request().get(RemoteIssueLink.class);
    }

    /**
     * Returns the remote links for an issue.
     *
     * @param issueKey the issue key
     * @return a List of RemoteIssueLinks
     */
    public List<RemoteIssueLink> getRemoteIssueLinks(final String issueKey)
    {
        return getRemoteIssueLinkResource(issueKey, null, null).request().get(RemoteIssueLink.REMOTE_ISSUE_LINKS_TYPE);
    }

    /**
     * Returns the remote link for an issue with the given globalId.
     *
     * @param issueKey the issue key
     * @param globalId the global ID
     * @return a RemoteIssueLink
     */
    public RemoteIssueLink getRemoteIssueLinkByGlobalId(final String issueKey, final String globalId)
    {
        final Map<String, String> params = MapBuilder.<String, String>newBuilder()
                .add("globalId", globalId)
                .toMap();
        return getRemoteIssueLinkResource(issueKey, null, params).request().get(RemoteIssueLink.class);
    }

    /**
     * Gets a remote link, returning a Response.
     *
     * @param issueKey the issue key
     * @param remoteIssueLinkId the remote issue link ID
     * @return a Response
     */
    public ParsedResponse getRemoteIssueLinkResponse(final String issueKey, final String remoteIssueLinkId)
    {
        return toResponse(() ->  getRemoteIssueLinkResource(issueKey, remoteIssueLinkId, null).request()
                .get(Response.class));
    }

    /**
     * Gets the remote links for an issue, returning a Response.
     *
     * @param issueKey the issue key
     * @return a Response
     */
    public ParsedResponse getRemoteIssueLinksResponse(final String issueKey)
    {
        return getRemoteIssueLinksResponse(issueKey, null);
    }

    public ParsedResponse getRemoteIssueLinksResponse(final String issueKey, @Nullable final Map<String, String> queryParams)
    {
        return toResponse(() ->  getRemoteIssueLinkResource(issueKey, null, queryParams).request()
                .get(Response.class));
    }

    /**
     * Archives the issue associated with the passed id or key
     *
     * @param issueIdOrKey Issue id or issue key
     * @return no content response
     * @response.representation.204.mediaType application/json
     * @response.representation.204.doc Returned if the issue is successfully archived.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to archive the issue or
     * doesn't have DC license or issue is already archived.
     * @response.representation.404.doc Returned if the issue does not exist.
     */
    public Response archive(final String issueIdOrKey)
    {
        final Response response = createResource()
                .path(issueIdOrKey)
                .path("archive")
                .request()
                .put(null, Response.class);
        response.close();

        return response;
    }

    /**
     * Restores the issue associated with the passed id or key
     *
     * @param issueIdOrKey Issue id or issue key
     * @return no content response
     * @response.representation.204.mediaType application/json
     * @response.representation.204.doc Returned if the issue is successfully restored.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to restore the issue or
     * doesn't have DC license or issue is not archived.
     * @response.representation.404.doc Returned if the issue does not exist.
     */
    public Response restore(final String issueIdOrKey)
    {
        final Response response = createResource()
                .path(issueIdOrKey)
                .path("restore")
                .request()
                .put(null, Response.class);
        response.close();

        return response;
    }

    public ParsedResponse<List<Attachment>> attachFile(String issueIdOrKey, File file) {
        return toResponse(() -> {

            final MultiPart multiPartEntity = new MultiPart(MediaType.MULTIPART_FORM_DATA_TYPE)
                    .bodyPart(new FileDataBodyPart("file", file, MediaType.APPLICATION_OCTET_STREAM_TYPE));

            return createResource().path(issueIdOrKey).path("attachments")
                    .request()
                    .header("X-Atlassian-Token", "nocheck")
                    .post(entity(multiPartEntity, multiPartEntity.getMediaType()), Response.class);
        }, new GenericType<List<Attachment>>() {});
    }

    /**
     * Returns WebTarget, containing remote link info.
     *
     * @param issueKey the issue key
     * @param remoteIssueLinkId the id of the remote link, if null get all links
     * @return a WebTarget
     */
    private WebTarget getRemoteIssueLinkResource(final String issueKey, @Nullable final String remoteIssueLinkId, @Nullable final Map<String, String> queryParams)
    {
        WebTarget resource = createResource().path(issueKey).path("remotelink");

        if (remoteIssueLinkId != null)
        {
            resource = resource.path(remoteIssueLinkId);
        }

        if (queryParams != null)
        {
            for (final Map.Entry<String, String> entry : queryParams.entrySet())
            {
                resource = resource.queryParam(entry.getKey(), entry.getValue());
            }
        }

        return resource;
    }

    private WebTarget addStringListsToQueryParams(WebTarget resource, final String paramName, final Iterable<StringList> stringLists)
    {
        if (stringLists != null)
        {
            for (StringList stringList : stringLists)
            {
                resource = resource.queryParam(paramName, stringList.toQueryParam());
            }
        }

        return resource;
    }

    private WebTarget addStringsToQueryParams(WebTarget resource, final String paramName, final Iterable<String> strings)
    {
        if (strings != null)
        {
            for (String string: strings)
            {
                resource = resource.queryParam(paramName, string);
            }
        }

        return resource;
    }

    private WebTarget createResourceWithIssueKey(String issueKey)
    {
        return createResource().path(issueKey);
    }


    @Override
    protected WebTarget createResource()
    {
        return super.createResource().path("issue");
    }

    @Override
    protected WebTarget createResource(Client client)
    {
        return super.createResource(client).path("issue");
    }
}
