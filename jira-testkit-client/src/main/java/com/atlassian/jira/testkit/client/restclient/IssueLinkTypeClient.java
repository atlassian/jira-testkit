/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.client.Entity.json;

/**
 * @since v4.3
 */
public class IssueLinkTypeClient extends RestApiClient<IssueLinkTypeClient> {
    public IssueLinkTypeClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public IssueLinkTypeClient(JIRAEnvironmentData environmentData, String version) {
        super(environmentData, version);
    }

    public IssueLinkTypes getIssueLinkTypes() {
        return issueLinkType().request().get(IssueLinkTypes.class);
    }

    private WebTarget issueLinkType() {
        return createResource().path("issueLinkType");
    }

    public IssueLinkType getIssueLinkType(String issueLinkTypeID) {
        return issueLinkTypeID(issueLinkTypeID).request().get(IssueLinkType.class);
    }

    public ParsedResponse deleteIssueLinkType(final String issueLinkTypeID) {
        return toResponse(() -> issueLinkTypeID(issueLinkTypeID).request().delete(Response.class));
    }

    public IssueLinkType createIssueLinkType(final String name, final String inbound, final String outbound) {
        final IssueLinkType linkType = new IssueLinkType();
        linkType.inward = inbound;
        linkType.outward = outbound;
        linkType.name = name;

        return issueLinkType().request(MediaType.APPLICATION_JSON_TYPE).post(json(linkType), IssueLinkType.class);
    }

    private WebTarget issueLinkTypeID(String issueLinkTypeID) {
        return createResource().path("issueLinkType/" + issueLinkTypeID);
    }

    public ParsedResponse getResponseForLinkType(final String issueLinkTypeID) {
        return toResponse(() -> issueLinkTypeID(issueLinkTypeID).request().get(Response.class));
    }

    public ParsedResponse getResponseForAllLinkTypes() {
        return toResponse(() -> issueLinkType().request().get(Response.class));
    }

    public IssueLinkType updateIssueLinkType(String issueLinkTypeID, String name, String inward, String outward) {
        IssueLinkType issueLinkType = new IssueLinkType();
        issueLinkType.name = name;
        issueLinkType.inward = inward;
        issueLinkType.outward = outward;
        return createResource().path("issueLinkType/" + issueLinkTypeID).request(MediaType.APPLICATION_JSON_TYPE).put(json(issueLinkType), IssueLinkType.class);
    }

    public ParsedResponse<IssueLinkType> moveIssueLinkType(String issueLinkTypeID,
                                                           IssueLinkTypeOrderUpdateRequest request) {
        return toResponse(() -> {
            final String path = String.format("issueLinkType/%s/order", issueLinkTypeID);
            return createResource().path(path).request(MediaType.APPLICATION_JSON_TYPE).put(json(request), Response.class);
        }, IssueLinkType.class);
    }

    public ParsedResponse<IssueLinkTypes> resetOrderAlphabetically(IssueLinkTypeResetOrderRequest request) {
        return toResponse(() -> createResource().path("issueLinkType/order").request(MediaType.APPLICATION_JSON_TYPE).put(json(request), Response.class), IssueLinkTypes.class);
    }
}
