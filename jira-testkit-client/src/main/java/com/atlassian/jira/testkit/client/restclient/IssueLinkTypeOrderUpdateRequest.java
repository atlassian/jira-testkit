package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public class IssueLinkTypeOrderUpdateRequest {

    private final Long newPosition;

    @JsonCreator
    public IssueLinkTypeOrderUpdateRequest(Long newPosition) {
        this.newPosition = newPosition;
    }

    public Long getNewPosition() {
        return newPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IssueLinkTypeOrderUpdateRequest that = (IssueLinkTypeOrderUpdateRequest) o;
        return Objects.equals(newPosition, that.newPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(newPosition);
    }
}
