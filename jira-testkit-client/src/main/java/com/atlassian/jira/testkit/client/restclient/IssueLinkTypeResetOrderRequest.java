package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public class IssueLinkTypeResetOrderRequest {
    private final String direction;

    @JsonCreator
    public IssueLinkTypeResetOrderRequest(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IssueLinkTypeResetOrderRequest that = (IssueLinkTypeResetOrderRequest) o;
        return Objects.equals(direction, that.direction);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(direction);
    }
}
