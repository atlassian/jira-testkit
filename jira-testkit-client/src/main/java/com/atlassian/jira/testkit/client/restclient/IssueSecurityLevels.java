package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.beans.IssueSecurityType;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class IssueSecurityLevels {
    List<IssueSecurityType> levels;

    public List<IssueSecurityType> getLevels() {
        return levels;
    }

    public void setLevels(List<IssueSecurityType> levels) {
        this.levels = levels;
    }

    public IssueSecurityLevels levels(List<IssueSecurityType> levels)
    {
        this.setLevels(levels);
        return this;
    }
}
