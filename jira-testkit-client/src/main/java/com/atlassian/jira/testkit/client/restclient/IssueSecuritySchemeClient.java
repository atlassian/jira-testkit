package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.beans.IssueSecuritySchemeBean;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class IssueSecuritySchemeClient extends RestApiClient<IssueSecuritySchemeClient>
{

    public IssueSecuritySchemeClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public ParsedResponse<IssueSecuritySchemes> getAllSecuritySchemes() throws WebApplicationException
    {
        return toResponse(() -> resource().request().get(Response.class), IssueSecuritySchemes.class);
    }

    public ParsedResponse<IssueSecuritySchemeBean> get(final long schemeId) throws WebApplicationException
    {
        return toResponse(() -> issueSecuritySchemeWithID(schemeId).request().get(Response.class), IssueSecuritySchemeBean.class);
    }

    public ParsedResponse<IssueSecuritySchemeBean> getForProject(final String projectIdOrKey)
    {
        return toResponse(() ->  createResource()
                        .path("project")
                        .path(projectIdOrKey)
                        .path("issuesecuritylevelscheme")
                        .request()
                        .get(Response.class), IssueSecuritySchemeBean.class);
    }

    private WebTarget issueSecuritySchemeWithID(long schemeId)
    {
        return resource().path(String.valueOf(schemeId));
    }

    private WebTarget resource()
    {
        return createResource().path("issuesecurityschemes");
    }
}
