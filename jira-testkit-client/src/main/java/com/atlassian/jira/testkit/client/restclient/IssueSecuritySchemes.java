package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.beans.IssueSecuritySchemeBean;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class IssueSecuritySchemes {
    private List<IssueSecuritySchemeBean> issueSecuritySchemes;

    public List<IssueSecuritySchemeBean> getIssueSecuritySchemes()
    {
        return issueSecuritySchemes;
    }
}
