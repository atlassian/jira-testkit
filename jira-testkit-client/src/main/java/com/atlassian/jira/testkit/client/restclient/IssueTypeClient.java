/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import io.atlassian.fugue.Option;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static javax.ws.rs.client.Entity.json;

/**
 * Client for issue type.
 *
 * @since v4.3
 */
public class IssueTypeClient extends RestApiClient<IssueTypeClient> {
    /**
     * Constructs a new IssueTypeClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public IssueTypeClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * GETs the issue type with the given id.
     *
     * @return an IssueType
     * @throws javax.ws.rs.WebApplicationException if there is a problem getting the issue type
     */
    public List<IssueType> get() throws WebApplicationException {
        return issueTypes().get(new GenericType<List<IssueType>>() {
        });
    }

    /**
     * GETs the issue type with the given id.
     *
     * @param issueTypeID a String containing the issue type id
     * @return an IssueType
     * @throws WebApplicationException if there is a problem getting the issue type
     */
    public IssueType get(String issueTypeID) throws WebApplicationException {
        return issueTypeWithID(issueTypeID).request().get(IssueType.class);
    }

    public List<IssueType> getAlternatives(final String issueTypeId) {
        return issueTypeWithID(issueTypeId).path("alternatives").request().get(new GenericType<List<IssueType>>() {
        });
    }

    public IssueType post(IssueTypeCreateBean issueTypeCreateBean) {
        return issueTypes().post(json(issueTypeCreateBean), IssueType.class);
    }

    public IssueType update(final String issueTypeId, IssueTypeUpdateBean issueTypeUpdateBean) {
        return issueTypeWithID(issueTypeId).request(MediaType.APPLICATION_JSON_TYPE)
                .put(json(issueTypeUpdateBean), IssueType.class);
    }

    public ParsedResponse<Object> delete(final String issueTypeId, final Option<String> alternativeIssueTypeId) {
        final WebTarget webTarget = issueTypeWithID(issueTypeId);
        return toResponse(() -> alternativeIssueTypeId.fold(
                () -> webTarget,
                alternativeId -> webTarget.queryParam("alternativeIssueTypeId", alternativeId)
        ).request().delete());
    }

    /**
     * GETs the issue type with the given id, returning a Response.
     *
     * @param issueTypeID a String containing the issue type id
     * @return a Response
     */
    public ParsedResponse<Object> getResponse(final String issueTypeID) {
        return toResponse(() -> issueTypeWithID(issueTypeID).request().get(Response.class));
    }

    /**
     * Returns paginated list of filtered issue types
     *
     * @param startAt    the index of the first issue type to return
     * @param maxResults the maximum number of issue type to return
     * @param query      the string that issue type names will be matched with
     * @param projectIds the set of project ids by which issueTypes will be filtered
     */
    public PageBean<IssueType> getPage(final Long startAt, final Integer maxResults, final String query, final Set<String> projectIds) {
        WebTarget webTarget = this.createResource()
                .path("issuetype")
                .path("page")
                .queryParam("startAt", startAt.toString())
                .queryParam("maxResults", maxResults.toString())
                .queryParam("query", query);

        for (String projectId : projectIds) {
            webTarget = webTarget.queryParam("projectIds", projectId);
        }
        return webTarget.request().get(new GenericType<PageBean<IssueType>>() {
        });
    }

    /**
     * Creates a WebTarget for all issue types.
     *
     * @return a WebTarget
     */
    private Invocation.Builder issueTypes() {
        return createResource().path("issuetype").request(MediaType.APPLICATION_JSON_TYPE);
    }

    /**
     * Creates a WebTarget for the issue type with the given id.
     *
     * @param issueTypeID a String containing the issue type id
     * @return a WebTarget
     */
    private WebTarget issueTypeWithID(String issueTypeID) {
        return createResource().path("issuetype").path(issueTypeID);
    }
}
