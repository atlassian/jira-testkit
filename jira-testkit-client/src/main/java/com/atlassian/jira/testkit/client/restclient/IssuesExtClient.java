package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import javax.ws.rs.client.WebTarget;
import java.util.Date;

public class IssuesExtClient extends BackdoorControl<IssuesExtClient> {
    public IssuesExtClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public void touch(final String key) {
        createResource()
                .path("touch")
                .queryParam("key", key)
                .request()
                .put(null);
    }

    public void changeUpdated(final String key, final Date date) {
        createResource()
                .path("changeUpdated")
                .queryParam("key", key)
                .queryParam("date", Long.toString(date.getTime()))
                .request()
                .put(null);
    }

    public void changeCreated(final String key, final Date date) {
        createResource()
                .path("changeCreated")
                .queryParam("key", key)
                .queryParam("date", Long.toString(date.getTime()))
                .request()
                .put(null);
    }

    @Override
    protected WebTarget createResource() {
        return super.createResource().path("issues");
    }
}
