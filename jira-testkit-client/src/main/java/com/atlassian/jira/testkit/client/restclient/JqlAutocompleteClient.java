package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * Client for querying JQL Autocomplete (to test for suggestions based on field and partially typed value).
 *
 * @since v5.0.29
 */
public class JqlAutocompleteClient extends RestApiClient<JqlAutocompleteClient>
{
    /**
     * Constructs a new JqlAutocompleteClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public JqlAutocompleteClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData, "1.0");
    }

    /**
     * Get suggestions for the field and partially typed value.
     *
     * @param fieldName the field name
     * @param fieldValue the partial value
     * @return results object
     */
    public JqlAutocompleteResults getAutocomplete(final String fieldName, final String fieldValue)
    {
        return autocompleteResource(fieldName, fieldValue).request().get(JqlAutocompleteResults.class);
    }

    /**
     * Get response of call to autocomplete end point.
     * @param fieldName the field name
     * @param fieldValue the partial value
     * @return response object
     * @see #getAutocomplete(String, String)
     */
    public ParsedResponse getAutocompleteResponse(final String fieldName, final String fieldValue)
    {
        return toResponse(() ->  autocompleteResource(fieldName, fieldValue).request().get(Response.class));
    }

    /**
     * Creates a WebTarget.Builder for the autocomplete resource.
     *
     * @return a WebTarget.Builder
     */
    private WebTarget autocompleteResource(String fieldName, String fieldValue)
    {
        return createResource()
                .path("jql").path("autocomplete")
                .queryParam("fieldName", fieldName)
                .queryParam("fieldValue", fieldValue);
    }
}
