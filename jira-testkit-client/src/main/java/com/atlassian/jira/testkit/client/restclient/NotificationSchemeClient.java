package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;

public class NotificationSchemeClient extends RestApiClient<NotificationSchemeClient>
{
    public NotificationSchemeClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public NotificationSchemeBean getNotificationScheme(Long notificationSchemeId, final String expand)
    {
        WebTarget webTarget = getWebTarget(expand)
                .path(notificationSchemeId.toString());

        return webTarget.request().get(NotificationSchemeBean.class);
    }

    public NotificationSchemePageBean getNotificationSchemes(final Integer startAt,
            final Integer maxResults,
            final String expand)
    {
        WebTarget webTarget = getWebTarget(expand);
        if (startAt != null)
        {
            webTarget = webTarget.queryParam("startAt", startAt.toString());
        }
        if (maxResults != null)
        {
            webTarget = webTarget.queryParam("maxResults", maxResults.toString());
        }
        return webTarget.request().get(NotificationSchemePageBean.class);
    }

    private WebTarget getWebTarget(String expand)
    {
        WebTarget webTarget = createResource()
                .path("notificationscheme");
        if (expand != null)
        {
            webTarget = webTarget.queryParam("expand", expand);
        }
        return webTarget;
    }
}
