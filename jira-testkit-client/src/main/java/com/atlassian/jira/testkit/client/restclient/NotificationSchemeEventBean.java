package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationSchemeEventBean
{
    @JsonProperty
    private NotificationEventBean event;
    @JsonProperty
    private List<NotificationBean> notifications;

    public NotificationSchemeEventBean()
    {
    }

    public NotificationSchemeEventBean(final NotificationEventBean event, final List<NotificationBean> notifications)
    {
        this.event = event;
        this.notifications = notifications;
    }

    public NotificationEventBean getEvent()
    {
        return event;
    }

    public List<NotificationBean> getNotifications()
    {
        return notifications;
    }
}
