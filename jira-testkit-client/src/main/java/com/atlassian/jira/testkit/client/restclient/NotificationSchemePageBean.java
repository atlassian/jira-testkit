package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class NotificationSchemePageBean extends PageBean<NotificationSchemeBean>
{
    public NotificationSchemePageBean()
    {
    }

    public NotificationSchemePageBean(final List<NotificationSchemeBean> values, final long total, final int maxResults, final long startAt)
    {
        super(values, total, maxResults, startAt);
    }
}
