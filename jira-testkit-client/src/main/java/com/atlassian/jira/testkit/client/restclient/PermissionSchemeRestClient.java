package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.beans.PermissionGrantBean;
import com.atlassian.jira.testkit.beans.PermissionSchemeAttributeBean;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Joiner;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

import static javax.ws.rs.client.Entity.json;

@ParametersAreNonnullByDefault
public final class PermissionSchemeRestClient extends RestApiClient<PermissionSchemeRestClient>
{
    public enum Expand {
        permissions, user, group, projectRole, field, all
    }

    public PermissionSchemeRestClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public ParsedResponse<PermissionSchemeListBean> getSchemes(final Expand... expands)
    {
        return toResponse(() ->  expandQuery(resource(), expands).request().get(Response.class),
                new GenericType<PermissionSchemeListBean>() {});
    }

    public ParsedResponse<PermissionSchemeBean> createScheme(final PermissionSchemeBean bean, final Expand... expands)
    {
        return toResponse(() ->  expandQuery(resource(), expands).request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(bean), Response.class),
            PermissionSchemeBean.class);
    }

    public ParsedResponse<PermissionSchemeBean> getScheme(final Long id, final Expand... expands)
    {
        return toResponse(() ->  expandQuery(resource().path(id.toString()), expands).request()
                .get(Response.class),
            PermissionSchemeBean.class);
    }

    public ParsedResponse<PermissionSchemeBean> updateScheme(final Long id, final PermissionSchemeBean updateBean, final Expand... expands)
    {
        return toResponse(() ->  expandQuery(resource()
                        .path(id.toString()), expands)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(json(updateBean), Response.class), PermissionSchemeBean.class);
    }

    public ParsedResponse<?> deleteScheme(final Long id)
    {
        return toResponse(() ->  resource().path(id.toString()).request().delete(Response.class));
    }

    public ParsedResponse<PermissionGrantListBean> getPermissions(final Long schemeId, final Expand... expands)
    {
        return toResponse(() ->  expandQuery(resource().path(schemeId.toString()).path("permission"), expands)
                        .request().get(Response.class),
                new GenericType<PermissionGrantListBean>() {});
    }

    public ParsedResponse<PermissionGrantBean> createPermission(final Long schemeId, final PermissionGrantBean bean, final Expand... expands)
    {
        return toResponse(() ->  expandQuery(resource()
                        .path(schemeId.toString())
                        .path("permission"), expands)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(bean), Response.class), PermissionGrantBean.class);
    }

    public ParsedResponse<PermissionGrantBean> getPermission(final Long schemeId, final Long permissionId, final Expand... expands)
    {
        return toResponse(() ->  expandQuery(resource()
                        .path(schemeId.toString())
                        .path("permission")
                        .path(permissionId.toString()), expands)
                        .request()
                        .get(Response.class), PermissionGrantBean.class);
    }

    public ParsedResponse<?> deletePermission(final Long schemeId, final Long permissionId)
    {
        return toResponse(() ->  resource()
                        .path(schemeId.toString())
                        .path("permission")
                        .path(permissionId.toString())
                        .request()
                        .delete(Response.class));
    }

    public ParsedResponse<PermissionSchemeBean> getAssignedScheme(final String projectKeyOrId)
    {
        return toResponse(() ->  projectResource(projectKeyOrId).request().get(Response.class),
                PermissionSchemeBean.class);
    }

    public ParsedResponse<PermissionSchemeBean> assignScheme(final String projectKeyOrId, final Long schemeId)
    {
        return toResponse(() ->  projectResource(projectKeyOrId).request(MediaType.APPLICATION_JSON_TYPE)
                        .put(json(Collections.singletonMap("id", schemeId)), Response.class),
                PermissionSchemeBean.class);
    }

    public ParsedResponse<PermissionSchemeAttributeBean> getAttribute(final Long schemeId, final String attributeKey) {
        return toResponse(() -> resource()
                .path(schemeId.toString())
                .path("attribute")
                .path(attributeKey)
                .request()
                .get(Response.class), PermissionSchemeAttributeBean.class);
    }

    public ParsedResponse<?> setAttribute(final Long schemeId, final PermissionSchemeAttributeBean attribute) {
        return toResponse(() -> resource()
                .path(schemeId.toString())
                .path("attribute")
                .path(attribute.getKey())
                .request()
                .put(Entity.text(attribute.getValue()), Response.class));
    }


    private WebTarget resource()
    {
        return createResource().path("permissionscheme");
    }

    private WebTarget projectResource(String projectKeyOrId)
    {
        return createResource().path("project").path(projectKeyOrId).path("permissionscheme");
    }

    private WebTarget expandQuery(final WebTarget resource, final Expand[] expands)
    {
        return expands.length > 0 ? resource.queryParam("expand", Joiner.on(',').join(expands)) : resource;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PermissionGrantListBean
    {
        @JsonProperty
        public List<PermissionGrantBean> permissions;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PermissionSchemeListBean
    {
        @JsonProperty
        public List<PermissionSchemeBean> permissionSchemes;
    }
}
