/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v4.3
 */
public class PriorityClient extends RestApiClient<PriorityClient>
{
    /**
     * Constructs a new PriorityClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public PriorityClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs all priorities
     *
     * @return a list of Priority
     * @throws javax.ws.rs.WebApplicationException if anything goes wrong
     */
    public List<Priority> get() throws WebApplicationException
    {
        return priority().request().get(new GenericType<List<Priority>>(){});
    }

    /**
     * GETs the priorities, returning a Response object.
     *
     * @return a Response
     */
    public ParsedResponse getResponse() {
        return this.toResponse(() -> priority().request().get(Response.class));
    }

    /**
     * GETs the priority with the given ID.
     *
     * @param priorityID a String containing a priority ID
     * @return a Priority
     * @throws WebApplicationException if anything goes wrong
     */
    public Priority get(String priorityID) throws WebApplicationException
    {
        return priorityWithID(priorityID).request().get(Priority.class);
    }

    /**
     * GETs the priority with the given ID, returning a Response object.
     *
     * @param priorityID a String containing a priority ID
     * @return a Response
     */
    public ParsedResponse getResponse(final String priorityID)
    {
        return this.toResponse(() -> priorityWithID(priorityID).request().get(Response.class));
    }

    /**
     * Returns a WebTarget for priorities.
     *
     * @return a WebTarget
     */
    protected WebTarget priority()
    {
        return createResource().path("priority");
    }

    /**
     * Returns a WebTarget for the priority having the given ID.
     *
     * @param priorityID a String containing a priority ID
     * @return a WebTarget
     */
    protected WebTarget priorityWithID(String priorityID)
    {
        return createResource().path("priority").path(priorityID);
    }
}
