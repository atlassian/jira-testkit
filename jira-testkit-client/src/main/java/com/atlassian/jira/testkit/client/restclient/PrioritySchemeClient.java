package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

import static java.util.Collections.singletonMap;
import static javax.ws.rs.client.Entity.json;

public class PrioritySchemeClient extends RestApiClient<PrioritySchemeClient> {

    public PrioritySchemeClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public ParsedResponse<PrioritySchemeBean> createWithDefaultMapping(PrioritySchemeUpdateBean bean) {
        return toResponse(
                () -> resource()
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(bean), Response.class),
                PrioritySchemeBean.class
        );
    }

    public ParsedResponse<PrioritySchemeBean> updateWithDefaultMapping(PrioritySchemeUpdateBean bean, PrioritySchemeBean.Expand... expand) {
        return toResponse(
                () -> schemeResource(bean.getId(), expand)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(json(bean), Response.class),
                PrioritySchemeBean.class
        );
    }

    public ParsedResponse delete(long schemeId) {
        return toResponse(() -> schemeResource(schemeId).request().delete(Response.class));
    }

    public ParsedResponse<PrioritySchemeBean> get(long schemeId, PrioritySchemeBean.Expand... expand) {
        return toResponse(
                () -> schemeResource(schemeId, expand).request().get(Response.class),
                PrioritySchemeBean.class
        );
    }

    public ParsedResponse<PrioritySchemeGetAllResponseBean> getAll(final Long startAt, final Integer maxResults, PrioritySchemeGetAllResponseBean.Expand... expand) {
        return toResponse(
                () -> {
                    WebTarget webTarget = expandedResource(expand);
                    if (Objects.nonNull(startAt)) {
                        webTarget = webTarget.queryParam("startAt", startAt.toString());
                    }
                    if (Objects.nonNull(maxResults)) {
                        webTarget = webTarget.queryParam("maxResults", maxResults.toString());
                    }
                    return webTarget.request().get(Response.class);
                },
                PrioritySchemeGetAllResponseBean.class
        );
    }

    public ParsedResponse<PrioritySchemeBean> assign(long schemeId, String projectKey, PrioritySchemeBean.Expand... expand) {
        return toResponse(
                () -> expanded(projectSchemeResource(projectKey), setOf(PrioritySchemeBean.Expand.class, expand))
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(json(singletonMap("id", schemeId)), Response.class),
                PrioritySchemeBean.class
        );
    }

    public ParsedResponse<PrioritySchemeBean> unassign(long schemeId, String projectKey, PrioritySchemeBean.Expand... expand) {
        return toResponse(
                () -> expanded(projectSchemeResource(projectKey)
                        .path(Long.toString(schemeId)), setOf(PrioritySchemeBean.Expand.class, expand))
                        .request().delete(Response.class),
                PrioritySchemeBean.class
        );
    }

    public ParsedResponse<PrioritySchemeBean> getForProject(String projectKey, PrioritySchemeBean.Expand... expand) {
        return toResponse(
                () -> expanded(projectSchemeResource(projectKey), setOf(PrioritySchemeBean.Expand.class, expand))
                        .request().get(Response.class),
                PrioritySchemeBean.class
        );
    }

    private WebTarget expandedResource(PrioritySchemeBean.Expand... expand) {
        return expanded(resource(), setOf(PrioritySchemeBean.Expand.class, expand));
    }

    private WebTarget expandedResource(PrioritySchemeGetAllResponseBean.Expand... expand) {
        return expanded(resource(), setOf(PrioritySchemeGetAllResponseBean.Expand.class, expand));
    }

    private WebTarget resource() {
        return createResource().path("priorityschemes");
    }

    private WebTarget schemeResource(long schemeId, PrioritySchemeBean.Expand... expand) {
        return expanded(resource().path(Long.toString(schemeId)), setOf(PrioritySchemeBean.Expand.class, expand));
    }

    private WebTarget projectSchemeResource(String projectKey) {
        return createResource().path("project").path(projectKey).path("priorityscheme");
    }
}
