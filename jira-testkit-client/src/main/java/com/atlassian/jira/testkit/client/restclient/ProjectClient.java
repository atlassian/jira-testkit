/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static javax.ws.rs.client.Entity.json;

/**
 * Client class for the Project resource.
 *
 * @since v4.3
 */
public class ProjectClient extends RestApiClient<ProjectClient>
{

    public static class UpdateBean
    {
        private final Map<String, String> json;

        private UpdateBean(Map<ProjectUpdateField, String> fieldsToUpdate)
        {
            ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
            for (ProjectUpdateField field : fieldsToUpdate.keySet())
            {
                mapBuilder.put(field.jsonFieldName(), fieldsToUpdate.get(field));
            }
            json = mapBuilder.build();
        }

        public Map<String, String> getJson()
        {
            return Maps.newHashMap(json);
        }

        public static UpdateBeanBuilder builder()
        {
            return new UpdateBeanBuilder(Collections.<ProjectUpdateField, String>emptyMap());
        }

        public static class UpdateBeanBuilder
        {
            private final Map<ProjectUpdateField, String> builder;

            public UpdateBeanBuilder(Map<ProjectUpdateField, String> builder)
            {
                this.builder = builder;
            }

            public UpdateBeanBuilder with(ProjectUpdateField field, Object value)
            {
                Map<ProjectUpdateField, String> newMap = ImmutableMap.<ProjectUpdateField, String>builder().putAll(builder).put(field, value.toString()).build();
                return new UpdateBeanBuilder(newMap);
            }

            public UpdateBean build()
            {
                return new UpdateBean(builder);
            }
        }
    }

    /**
     * Constructs a new ProjectClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public ProjectClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the project having the given key.
     *
     * @param projectKey a String containing the project key
     * @return a Project
     * @throws WebApplicationException if there is a problem
     */
    public Project get(String projectKey) throws WebApplicationException
    {
        return projectWithKey(projectKey).request().get(Project.class);
    }

    /**
     * GETs the project having the given key, with all fields expanded
     *
     * @param projectKey a String containing the project key
     * @return a Project
     * @throws WebApplicationException if there is a problem
     */
    public Project getAndExpandAll(String projectKey) throws WebApplicationException
    {
        return projectWithKey(projectKey).queryParam("expand", "description,lead,url,projectKeys")
                .request().get(Project.class);
    }

    /**
     * Creates a specified project
     *
     * @param project a Java class that will be JSON-ized and send to the server
     * @return a {@link Response} object
     */
    public Response create(Object project)
    {
        return registerResponse(projects().request(MediaType.APPLICATION_JSON_TYPE)
                .post(json(project), Response.class));
    }

    /**
     * Updates a specified project
     *
     * @param keyOrId key or id of project to update
     * @param newData object containing new values for fields
     * @return a {@link Response} object
     */
    public Response update(String keyOrId, UpdateBean newData)
    {
        return registerResponse(projectWithKey(keyOrId).request(MediaType.APPLICATION_JSON_TYPE)
                .put(json(newData.json), Response.class));
    }

    /**
     * Updates the type of a project.
     *
     * @param keyOrId key or id of project to update
     * @param newProjectTypeKey The key of the new project type
     * @return a {@link Response} object
     */
    public ParsedResponse<Project> updateProjectType(final String keyOrId, final String newProjectTypeKey)
    {
        return toResponse(() -> projectWithKey(keyOrId).path("type").path(newProjectTypeKey).request()
                .put(null, Response.class), Project.class);
    }

    /**
     * Deletes a specified project
     *
     * @param keyOrId key or id of project to delete
     * @return a {@link Response} object
     */
    public Response delete(String keyOrId)
    {
        return registerResponse(projectWithKey(keyOrId).request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE).delete(Response.class));
    }

    /**
     * GETs a list of projects, which are visible to the current user.
     *
     * @return a list of projects.
     */
    public List<Project> getProjects()
    {
        return projects().request().get(Project.PROJECTS_TYPE);
    }

    /**
     * GETs a list of projects, which are visible to the current user, possibly expanding one or more fields.
     *
     * @param expand a comma separated list of fields to expand.
     * @return a list of projects.
     * @since 7.0
     */
    public List<Project> getProjects(String expand)
    {
        return projects(expand).request().get(Project.PROJECTS_TYPE);
    }

    /**
     * GETs a list of current user recent projects, possibly expanding one or more fields.
     *
     * @param expand a comma separated list of fields to expand
     * @param count the number of recent projects to get
     * @return a list of projects.
     * @since 7.0
     */
    public Response getRecentProjects(String expand, int count)
    {
        return projects(expand).queryParam("recent", String.valueOf(count)).request().get(Response.class);
    }

    /**
     * GETs a list of current user recent projects.
     *
     * @param count the number of recent projects to get
     * @return a list of projects.
     * @since 7.0
     */
    public Response getRecentProjects(int count)
    {
        return registerResponse(projects().queryParam("recent", String.valueOf(count)).request().get(Response.class));
    }

    /**
     * GETs a list of versions, associated with the passed project.
     *
     * @param key the key of the project to query.
     * @return a list of versions.
     */
    public List<Version> getVersions(String key)
    {
        return projectVersionWithKey(key).request().get(Version.VERSIONS_TYPE);
    }

    /**
     * GETs a list of versions, associated with the passed project.
     *
     * @param key the key of the project to query.
     * @return a list of versions.
     */
    public PageBean<Version> getVersionsPaged(String key, final Long startAt, final Integer maxResults, final String orderBy)
    {
        WebTarget webTarget = projectVersionsPaged(key);
        if (startAt != null)
        {
            webTarget = webTarget.queryParam("startAt", startAt.toString());
        }
        if (maxResults != null)
        {
            webTarget = webTarget.queryParam("maxResults", maxResults.toString());
        }
        if (orderBy != null)
        {
            webTarget = webTarget.queryParam("orderBy", orderBy);
        }

        return webTarget.request().get(Version.VERSIONS_PAGED_TYPE);
    }

    /**
     * GETs a map of avatars, associated with the passed project.
     *
     * @param key the key of the project to query.
     * @return a map of avatars, system and custom
     */
    public Map<String, List<Avatar>> getAvatars(String key)
    {
        return projectWithKey(key).path("avatars").request().get(Avatar.ALLAVATARS_TYPE);
    }

    /**
     * GETs a single avatar, associated with the passed project
     *
     * @param key the key of the project to query
     * @param id  the database id of the avatar
     * @return avatar
     */
    public Avatar getAvatar(String key, Long id)
    {
        return projectWithKey(key).path("avatar").path(id.toString())
                .request().get(Avatar.AVATAR_TYPE);
    }

    /**
     * GETs the project having the given key, and returns a Response.
     *
     * @param projectKey a String containing the project key
     * @return a Response
     */
    public ParsedResponse getResponse(final String projectKey)
    {
        return toResponse(() -> projectWithKey(projectKey).request().get(Response.class));
    }

    /**
     * GETs the versions associated with the passed project.
     *
     * @param projectKey a String containing the project key
     * @return a Response
     */
    public ParsedResponse getVersionsResponse(final String projectKey)
    {
        return toResponse(() ->  projectWithKey(projectKey).request().get(Response.class));
    }

    /**
     * GETs a list of components, associated with the passed project.
     *
     * @param key the key of the project to query.
     * @return a list of components.
     */
    public List<Component> getComponents(String key)
    {
        return projectComponentWithKey(key).request().get(Component.COMPONENTS_TYPE);
    }

    /**
     * Marks the project associated with the passed id or key as archived
     *
     * @param projectIdOrKey the key or id of the project
     * @return no content response
     * @response.representation.204.mediaType application/json
     * @response.representation.204.doc Returned if the project is successfully archived.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to archive the project or
     * doesn't have DC license or project is already archived.
     * @response.representation.404.doc Returned if the project does not exist.
     */
    public Response archive(final String projectIdOrKey)
    {
        final Response response = projects()
                .path(projectIdOrKey)
                .path("archive")
                .request()
                .put(null, Response.class);
        response.close();

        return response;
    }

    /**
     * Restores the project associated with the passed id or key
     *
     * @param projectIdOrKey the key or id of the project
     * @return no content response
     * @response.representation.202.mediaType application/json
     * @response.representation.202.doc Returned if the project is successfully restored.
     * @response.representation.401.doc Returned if the user is not logged in.
     * @response.representation.403.doc Returned if the currently authenticated user does not have permission to restore the project or
     * doesn't have DC license or project is already restored.
     * @response.representation.404.doc Returned if the project does not exist.
     */
    public Response restore(final String projectIdOrKey)
    {
        final Response response = projects()
                .path(projectIdOrKey)
                .path("restore")
                .request()
                .put(null, Response.class);
        response.close();

        return response;
    }

    /**
     * Returns a WebTarget for the project having the given key.
     *
     * @param projectKey a String containing the project key
     * @return a Response
     */
    protected WebTarget projectWithKey(String projectKey)
    {
        return projects().path(projectKey);
    }

    /**
     * Returns a WebTarget for the project versions 
     *
     * @param projectKey a String containing the project key
     * @return a Response
     */
    protected WebTarget projectVersionWithKey(String projectKey)
    {
        return projectWithKey(projectKey).path("versions");
    }

    /**
     * Returns a WebTarget for the versions gi
     *
     * @param projectKey a String containing the project key
     * @return a Response
     */
    protected WebTarget projectVersionsPaged(String projectKey)
    {
        return projectWithKey(projectKey).path("version");
    }

    /**
     * Returns a WebTarget for the versions gi
     *
     * @param projectKey a String containing the project key
     * @return a Response
     */
    protected WebTarget projectComponentWithKey(String projectKey)
    {
        return projectWithKey(projectKey).path("components");
    }

    /**
     * Returns a WebTarget for the projects visible to the current user.
     *
     * @return a Response
     */
    protected WebTarget projects()
    {
        return createResource().path("project");
    }

    /**
     * Returns a WebTarget for the projects visible to the current user, optionally expanding fields.
     *
     * @param expand a comma separated list of fields to expand in the response
     * @return a Response
     */
    protected WebTarget projects(String expand)
    {
        return projects().queryParam("expand", expand);
    }
}
