/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.google.common.collect.Maps;

import javax.annotation.Nullable;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

import static javax.ws.rs.client.Entity.json;

/**
 * @since v4.4
 */
public class ProjectRoleClient extends RestApiClient<ProjectRoleClient>
{
    protected final ProjectRoleClient2 projectRoleClient2;

    public GenericType<Map<String, String>> TYPE = new GenericType<>(HashMap.class);

    public ProjectRoleClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        projectRoleClient2 = new ProjectRoleClient2(environmentData);
    }

    public Map<String, String> get(String projectKey) throws WebApplicationException
    {
        return rolesWithProjectKey(projectKey).request().get(TYPE);
    }

    public void deleteRole(String name) {
        projectRoleClient2.deleteRole(name);
    }

    public long addRole(String name, String description) {
        return projectRoleClient2.addRole(name, description);
    }

    public long addRole(String name) {
        return projectRoleClient2.addRole(name, "");
    }

    public ProjectRole get(String projectKey, String role)
    {
        final WebTarget webTarget = resourceRoot(get(projectKey).get(role));
        return webTarget.request().get(ProjectRole.class);
    }

    public ParsedResponse addActors(final String projectKey, final String role, @Nullable final String[] groupNames,
                                    @Nullable final String[] userNames)
    {
        final ProjectRole projectRole = get(projectKey, role);

        return toResponse(() -> {
            final WebTarget webTarget = rolesWithProjectKey(projectKey).path(projectRole.id.toString());
            final Map<String, String[]> parameter = Maps.newHashMap();
            if(groupNames != null)
            {
                parameter.put("group", groupNames);
            }
            if(userNames != null)
            {
                parameter.put("user", userNames);
            }
            return webTarget.request(MediaType.APPLICATION_JSON_TYPE).post(json(parameter), Response.class);
        });
    }

    public ParsedResponse deleteGroup(final String projectKey, final String role, final String groupName)
    {
        final ProjectRole projectRole = get(projectKey, role);

        return toResponse(() -> {
            final WebTarget webTarget = rolesWithProjectKey(projectKey).path(projectRole.id.toString()).queryParam("group", groupName);
            return webTarget.request().delete(Response.class);
        });
    }

    public ParsedResponse deleteUser(final String projectKey, final String role, final String userName)
    {
        final ProjectRole projectRole = get(projectKey, role);

        return toResponse(() -> {
            final WebTarget webTarget = rolesWithProjectKey(projectKey).path(projectRole.id.toString()).queryParam("user", userName);
            return webTarget.request().delete(Response.class);
        });
    }

    protected WebTarget rolesWithProjectKey(String projectKey)
    {
        return createResource().path("project").path(projectKey).path("role");
    }

    public ParsedResponse setActors(final String projectKey, final String role, final Map<String, String[]> actors)
    {
        final ProjectRole projectRole = get(projectKey, role);

        return toResponse(() -> {
            final WebTarget webTarget = rolesWithProjectKey(projectKey).path(projectRole.id.toString());
            final ProjectRoleActorsUpdate projectRoleActorsUpdate = new ProjectRoleActorsUpdate(
                    projectRole.id, actors
            );
            return webTarget.request(MediaType.APPLICATION_JSON_TYPE).put(json(projectRoleActorsUpdate), Response.class);
        });
    }
}
