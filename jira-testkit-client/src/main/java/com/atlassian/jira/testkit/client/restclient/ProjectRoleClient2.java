/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import javax.ws.rs.core.Response;

/**
 * @since v4.4
 */
public class ProjectRoleClient2 extends BackdoorControl<ProjectRoleClient2>
{
    public ProjectRoleClient2(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public void deleteRole(final String role) {
        createResource().path("projectRoles").path(role).request().delete();
    }

    public long addRole(final String roleName, final String description) {
        return createResource().path("projectRoles")
                .path("add")
                .path(roleName)
                .path(description)
                .request()
                .post(null, Long.class);
    }
}
