package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;

public class ProjectSecurityLevelClient extends RestApiClient<ProjectSecurityLevelClient>
{
    public ProjectSecurityLevelClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    protected WebTarget issueSecurityLevelsForProject(String projectKey)
    {
        return createResource().path("project").path(projectKey).path("securitylevel");
    }

    /**
     * GETs the list of issue security levels for the given project key.
     *
     * @param projectKey a String representing the project key
     * @return an IssueSecurityType
     * @throws WebApplicationException if there is a problem getting the issue type
     */
    public IssueSecurityLevels get(String projectKey) throws WebApplicationException
    {
        return issueSecurityLevelsForProject(projectKey).request().get(IssueSecurityLevels.class);
    }

    /**
     * GETs the issue type with the given id, returning a Response.
     *
     * @param projectKey a String representing the project key
     * @return a Response
     */
    public ParsedResponse getResponse(final String projectKey)
    {
        return issueSecurityLevelsForProject(projectKey).request().get(ParsedResponse.class);
    }

}
