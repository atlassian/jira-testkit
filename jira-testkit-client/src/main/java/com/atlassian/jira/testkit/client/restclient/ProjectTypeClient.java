package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.beans.ProjectTypeBean;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

public class ProjectTypeClient extends RestApiClient<ProjectTypeClient>
{
    public ProjectTypeClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public ParsedResponse<List<ProjectTypeBean>> getAllProjectTypes()
    {
        return toResponse(() ->  projectTypes().request().get(Response.class),
                new GenericType<List<ProjectTypeBean>>(){});
    }

    public ParsedResponse<ProjectTypeBean> getByKey(final String projectTypeKey)
    {
        return toResponse(() ->  projectTypes().path(projectTypeKey).request().get(Response.class),
                ProjectTypeBean.class);
    }

    public ParsedResponse<ProjectTypeBean> getAccessibleProjectTypeByKey(final String projectTypeKey)
    {
        return toResponse(() ->  projectTypes().path(projectTypeKey).path("accessible").request().get(Response.class),
                ProjectTypeBean.class);
    }

    protected WebTarget projectTypes()
    {
        return createResource().path("project/type");
    }
}
