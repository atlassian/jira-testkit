/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Client class for the Resolution resource.
 *
 * @since v4.3
 */
public class ResolutionClient extends RestApiClient<ResolutionClient>
{
    /**
     * Constructs a new ResolutionClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public ResolutionClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs all priorities
     *
     * @return a list of Resolution
     * @throws WebApplicationException if anything goes wrong
     */
    public List<Resolution> get() throws WebApplicationException
    {
        return resolution().request().get(new GenericType<List<Resolution>>(){});
    }

    /**
     * GETs the resolution having the given id.
     *
     * @param resolutionID a String containing the resolution id
     * @return a Resolution
     * @throws WebApplicationException if there is an error
     */
    public Resolution get(String resolutionID) throws WebApplicationException
    {
        return resolutionWithID(resolutionID).request().get(Resolution.class);
    }

    /**
     * GETs the resolution having the given id, and returns a Reponse.
     *
     * @param resolutionID a String containing the resolution id
     * @return a Response
     */
    public ParsedResponse getResponse(final String resolutionID)
    {
        return toResponse(() ->  resolutionWithID(resolutionID).request().get(Response.class));
    }

    /**
     * Creates a WebTarget for the resolution having the given id.
     *
     * @param resolutionID a String containing the resolution id
     * @return a WebTarget
     */
    private WebTarget resolutionWithID(String resolutionID)
    {
        return createResource().path("resolution").path(resolutionID);
    }

    /**
     * Creates a WebTarget for the resolutions
     *
     * @return a WebTarget
     */
    private WebTarget resolution()
    {
        return createResource().path("resolution");
    }
}
