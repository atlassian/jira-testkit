package com.atlassian.jira.testkit.client.restclient;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

/**
 * @deprecated Please use ParsedResponse class to avoid name clash with
 * javax.ws.rs.core.Response
 */
@Deprecated
public class Response<T> {
    public final int statusCode;
    public final Errors entity;
    public final T body;

    public Response(int statusCode, Errors entity) {
        this.statusCode = statusCode;
        this.entity = entity;
        this.body = null;
    }

    public Response(int statusCode, Errors entity, T body) {
        this.statusCode = statusCode;
        this.entity = entity;
        this.body = body;
    }

    @Override
    public boolean equals(Object obj)
    {
        return reflectionEquals(this, obj);
    }

    @Override
    public int hashCode()
    {
        return reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
