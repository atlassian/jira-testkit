package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static javax.ws.rs.client.Entity.json;

/**
 * @since 7.0
 */
public class RoleClient extends RestApiClient<RoleClient>
{
    public RoleClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public ProjectRole get(String roleKey)
    {
        return roles().path(roleKey).request().get(ProjectRole.class);
    }

    public List<ProjectRole> get()
    {
        return roles().request().get(new GenericType<List<ProjectRole>>(){});
    }

    public ProjectRole create(String name, String description)
    {
        return roles().request(MediaType.APPLICATION_JSON_TYPE)
                .post(json(new CreateProjectRoleBean(name, description)), ProjectRole.class);
    }

    public ProjectRole updatePartial(Long id, String name, String description)
    {
        return roles().path(String.valueOf(id)).request(MediaType.APPLICATION_JSON_TYPE)
                .post(json(new CreateProjectRoleBean(name, description)), ProjectRole.class);
    }

    public ProjectRole updateFull(Long id, String name, String description)
    {
        return roles().path(String.valueOf(id)).request(MediaType.APPLICATION_JSON_TYPE)
                .put(json(new CreateProjectRoleBean(name, description)), ProjectRole.class);
    }

    public void deleteProjectRole(Long id)
    {
        roles().path(String.valueOf(id)).request().delete(String.class);
    }

    public void deleteProjectRole(Long id, Long replacementId)
    {
        roles().path(String.valueOf(id)).queryParam("swap", String.valueOf(replacementId)).request().delete(String.class);
    }

    public ProjectRoleActorsBean getDefaultActorsForRole(Long id)
    {
        return roles().path(String.valueOf(id)).path("actors").request().get(ProjectRoleActorsBean.class);
    }

    public ProjectRoleActorsBean addDefaultActorsToRole(Long id, String[] usernames, String[] groupnames)
    {
        return roles()
                .path(String.valueOf(id)).path("actors")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(json(new ActorInputBean(
                                usernames == null ? null : Arrays.asList(usernames),
                                groupnames == null? null : Arrays.asList(groupnames))), ProjectRoleActorsBean.class);
    }

    public ProjectRoleActorsBean deleteDefaultActorsToRole(Long id, String username, String groupname)
    {
        WebTarget actors = roles().path(String.valueOf(id)).path("actors");
        if (username != null)
        {
            actors = actors.queryParam("user", username);
        }
        if (groupname != null)
        {
            actors = actors.queryParam("group", groupname);
        }
        return actors.request().delete(ProjectRoleActorsBean.class);
    }

    protected WebTarget roles()
    {
        return createResource().path("role");
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class CreateProjectRoleBean
    {
        @JsonProperty
        private String name;
        @JsonProperty
        private String description;

        public CreateProjectRoleBean(String name, String description)
        {
            this.name = name;
            this.description = description;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class ActorInputBean
    {
        @JsonProperty
        private Collection<String> user;
        @JsonProperty
        private Collection<String> group;

        @JsonCreator
        public ActorInputBean(@JsonProperty("user") Collection<String> usernames, @JsonProperty("group") Collection<String> groupnames)
        {
            this.user = usernames;
            this.group = groupnames;
        }
    }
}
