package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.ws.rs.core.GenericType;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Screen {
    public static final GenericType<List<Screen>> LIST = new GenericType<List<Screen>>() {
    };
    public Long id;
    public String name;
    public String description;
    public String expand;

    public Screen(Long id, String name, String description, String expand) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.expand = expand;
    }

    public Screen(Long id, String name, String description) {
        this(id, name, description, null);
    }

    public Screen() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Screen screen = (Screen) o;
        return new EqualsBuilder().append(id, screen.id).append(name, screen.name).append(description, screen.description).append(expand, screen.expand).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(name).append(description).append(expand).toHashCode();
    }

    @Override
    public String toString() {
        return "Screen{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", expand='" + expand + '\'' +
                '}';
    }
}
