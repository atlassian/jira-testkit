/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.client.Entity.json;

public class ScreensClient extends RestApiClient<ScreensClient>
{
    private final Long id;

    public ScreensClient(JIRAEnvironmentData environmentData, Long id)
    {
        super(environmentData);
        this.id = id;
    }

    public List<ScreenTab> getAllTabs()
    {
        return getAllTabs(null);
    }

    public List<ScreenTab> getAllTabs(String projectKey)
    {
        return getTabsResource(projectKey).request().get(ScreenTab.LIST);
    }

    public ParsedResponse getAllTabsResponse()
    {
        return getAllTabsResponse(null);
    }

    public ParsedResponse getAllTabsResponse(final String projectKey)
    {
        return toResponse(() ->  getTabsResource(projectKey).request().get(Response.class));
    }

    public ScreenTab createTab(String name)
    {
        final ScreenTab screenTab = new ScreenTab();
        screenTab.name = name;
        return getTabsResource()
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(json(screenTab), ScreenTab.class);
    }

    public ParsedResponse createTabWithResponse(String name)
    {
        final ScreenTab screenTab = new ScreenTab();
        screenTab.name = name;
        return toResponse(() ->  getTabsResource()
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(screenTab), Response.class));
    }


    public void deleteTab(Long id)
    {
        getTabsResource()
                .path("" + id)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .delete();
    }

    public ParsedResponse deleteTabWithResponse(final Long id)
    {
        return toResponse(() ->  getTabsResource()
                        .path("" + id)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .delete(Response.class));
    }

    public ScreenTab renameTab(Long id, String name)
    {
        final ScreenTab screenTab = new ScreenTab();
        screenTab.name = name;
        return getTabsResource()
                .path("" + id)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(json(screenTab), ScreenTab.class);
    }

    public ParsedResponse renameTabWithResponse(final Long id, final String name)
    {
        return toResponse(() -> {
            final ScreenTab screenTab = new ScreenTab();
            screenTab.name = name;
            return getTabsResource()
                    .path("" + id)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .put(json(screenTab), Response.class);
        });
    }

    public void moveTab(Long id, Integer pos)
    {
        getTabsResource()
                .path("" + id)
                .path("move")
                .path("" + pos)
                .request()
                .post(null);
    }

    public ParsedResponse moveTabWithResponse(final Long id, final Integer pos)
    {
        return toResponse(() ->  getTabsResource()
                        .path("" + id)
                        .path("move")
                        .path("" + pos)
                        .request()
                        .post(null, Response.class));
    }

    public ScreenField addField(Long tab, String field)
    {
        final AddField screenField = new AddField();
        screenField.fieldId = field;
        return getTabsResource()
                .path("" + tab)
                .path("fields")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(json(screenField), ScreenField.class);
    }

    public ParsedResponse addFieldWithResponse(final Long tab, final String field)
    {
        return toResponse(() -> {
            final AddField screenField = new AddField();
                screenField.fieldId = field;
                return getTabsResource()
                        .path("" + tab)
                        .path("fields")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(screenField), Response.class); });
    }

    public void moveField(Long tab, String fieldId, MoveField moveField)
    {
        getTabsResource()
                .path("" + tab)
                .path("fields")
                .path(fieldId)
                .path("move")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(json(moveField));
    }

    public ParsedResponse moveFieldWithResponse(final Long tab, final String fieldId, final MoveField moveField)
    {
        return toResponse(() ->  getTabsResource()
                        .path("" + tab)
                        .path("fields")
                        .path(fieldId)
                        .path("move")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(moveField), Response.class));
    }

    public void removeField(Long tab, String field)
    {
        getTabsResource()
                .path("" + tab)
                .path("fields")
                .path(field)
                .request()
                .delete();
    }

    public ParsedResponse removeFieldWithResponse(final Long tab, final String field)
    {
        return toResponse(() ->  getTabsResource()
                        .path("" + tab)
                        .path("fields")
                        .path(field)
                        .request()
                        .delete(Response.class));
    }

    public List<ScreenField> getAvailableFields()
    {
        return screen()
                .path("availableFields")
                .request()
                .get(ScreenField.LIST);
    }

    public ParsedResponse getFieldsResponse(Long tab)
    {
        return getFieldsResponse(tab, null);
    }

    public ParsedResponse getFieldsResponse(final Long tab, final String projectKey)
    {
        return toResponse(() ->  getTabsResource(projectKey)
                        .path("" + tab)
                        .path("fields")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get(Response.class));
    }

    public List<ScreenField> getFields(Long tab)
    {
        return getFields(tab, null);
    }

    public List<ScreenField> getFields(Long tab, String projectKey)
    {
        return getTabsResource(projectKey)
                .path("" + tab)
                .path("fields")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(ScreenField.LIST);
    }

    public ParsedResponse getAllScreensResponse() {
        return toResponse(() -> createResource().path("screens").request().get(Response.class));
    }

    public List<Screen> getAllScreens() {
        return createResource().path("screens").request().get(Screen.LIST);
    }

    protected WebTarget screen()
    {
        return createResource().path("screens").path("" + this.id);
    }

    private WebTarget getTabsResource() {return getTabsResource(null);}

    private WebTarget getTabsResource(String projectKey)
    {
        WebTarget screenWebTarget = screen();
        if (projectKey != null)
        {
            screenWebTarget = screenWebTarget.queryParam("projectKey", projectKey);
        }
        return screenWebTarget.path("tabs");
    }
}
