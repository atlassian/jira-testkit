/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Client for the status resource.
 *
 * @since v4.3
 */
public class StatusClient extends RestApiClient<StatusClient>
{
    /**
     * Constructs a new StatusClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public StatusClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the status with the given id.
     *
     * @param statusID a String containing the status id
     * @return a Status
     * @throws WebApplicationException if there's a problem getting the status
     */
    public Status get(String statusID) throws WebApplicationException
    {
        return statusWithID(statusID).request().get(Status.class);
    }

    /**
     * GETs all statuses
     *
     * @return a List of Statuses
     * @throws WebApplicationException if there's a problem getting the status
     */
    public List<Status> get() throws WebApplicationException
    {
        return status().request().get(new GenericType<List<Status>>(){});
    }

    /**
     * GETs the status with the given id, returning a Response object.
     *
     * @param statusID a String containing the status id
     * @return a Response
     */
    public ParsedResponse getResponse(final String statusID)
    {
        return toResponse(() ->  statusWithID(statusID).request().get(Response.class));
    }

    /**
     * Returns a WebTarget for the all status.
     *
     * @return a WebTarget
     */
    protected WebTarget status()
    {
        return createResource().path("status");
    }

    /**
     * Returns a WebTarget for the status having the given id.
     *
     * @param statusID a String containing the status id
     * @return a WebTarget
     */
    protected WebTarget statusWithID(String statusID)
    {
        return createResource().path("status").path(statusID);
    }
}
