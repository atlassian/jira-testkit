/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Client for the version resource.
 *
 * @since v4.3
 */
public class VersionClient extends RestApiClient<VersionClient>
{
    /**
     * Constructs a new VersionClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public VersionClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the version having a given id.
     *
     * @param versionID a String containing a version id
     * @return a Version
     * @throws WebApplicationException if anything goes wrong
     */
    public Version get(String versionID) throws WebApplicationException
    {
        return versionWithID(versionID).request().get(Version.class);
    }

    /**
     * GETs the version having a given id, returning a Response object.
     *
     * @param versionID a String containing a version id
     * @return a Response
     */
    public ParsedResponse getResponse(final String versionID)
    {
        return toResponse(() ->  versionWithID(versionID).request().get(Response.class));
    }

    public Version create(Version version) throws WebApplicationException
    {
        return version().post(json(version), Version.class);
    }

    public ParsedResponse createResponse(final Version version)
    {
        return toResponse(() -> version().post(json(version), Response.class));
    }

    public ParsedResponse delete(final String versionId) throws WebApplicationException
    {
        return delete(versionId, null, null);
    }

    public ParsedResponse delete(final String versionId, final URI swapFixVersion, final URI swapAffectedVersion) throws WebApplicationException
    {
        return toResponse(() -> {
                WebTarget deleteResource = versionWithID(versionId);
                if (swapFixVersion != null)
                {
                    deleteResource = deleteResource.queryParam("moveFixIssuesTo", swapFixVersion.getPath());
                }
                if (swapAffectedVersion != null)
                {
                    deleteResource = deleteResource.queryParam("moveAffectedIssuesTo", swapAffectedVersion.getPath());
                }
                return deleteResource.request().delete(Response.class);
            });
    }

    public ParsedResponse deleteVersionAndSwap(final String versionId, DeleteVersionWithCustomFieldParameters parameters) throws WebApplicationException
    {
        return toResponse((RestCall) () -> {
            WebTarget removeAndSwapResource = versionWithID(versionId).path("removeAndSwap");

            return removeAndSwapResource.request(MediaType.APPLICATION_JSON_TYPE)
                    .post(json(parameters), Response.class);
        });
    }

    public ParsedResponse merge(final String versionId, final String mergeToVersionId) throws WebApplicationException
    {
        return toResponse((RestCall) () -> {
            WebTarget mergeResource = versionWithID(versionId).path("mergeto").path(mergeToVersionId);

            return mergeResource.request().put(null, Response.class);
        });
    }

    public Version move(String versionId, VersionMove versionMove) throws WebApplicationException
    {
        return versionMove(versionId)
                .post(json(versionMove), Version.class);
    }

    public ParsedResponse moveResponse(final String versionId, final VersionMove versionMove)
    {
        return toResponse(() ->  versionMove(versionId)
                        .post(json(versionMove), Response.class));
    }

    public Version archive(String versionID) {
        return versionWithID(versionID).request(APPLICATION_JSON_TYPE)
                .put(json(new Version().archived(true)), Version.class);
    }

    public Version unarchive(String versionID) {
        return versionWithID(versionID).request(APPLICATION_JSON_TYPE)
                .put(json(new Version().archived(false)), Version.class);
    }

    public Version release(String versionID) {
        return versionWithID(versionID).request(APPLICATION_JSON_TYPE)
                .put(json(new Version().released(true)), Version.class);
    }

    public Version unrelease(String versionID) {
        return versionWithID(versionID).request(APPLICATION_JSON_TYPE)
                .put(json(new Version().released(false)), Version.class);
    }

    public VersionIssueCounts getVersionIssueCounts(String versionID) throws WebApplicationException
    {
        return versionWithID(versionID).path("relatedIssueCounts").request().get(VersionIssueCounts.class);
    }

    public ParsedResponse getVersionIssueCountsResponse(final String versionId)
    {
        return toResponse(() ->  versionWithID(versionId).path("relatedIssueCounts").request().get(Response.class));
    }

    public VersionUnresolvedIssueCount getVersionUnresolvedIssueCount(String versionID) throws WebApplicationException
    {
        return versionWithID(versionID).path("unresolvedIssueCount").request().get(VersionUnresolvedIssueCount.class);
    }

    public ParsedResponse getVersionUnresolvedIssueCountResponse(final String versionId)
    {
        return toResponse(() ->  versionWithID(versionId).path("unresolvedIssueCount").request().get(Response.class));
    }

    public ParsedResponse putResponse(final String versionId, final Version version)
    {
        return toResponse(() ->  versionWithID(versionId).request(APPLICATION_JSON_TYPE)
                        .put(json(version), Response.class));
    }

    public ParsedResponse putResponse(final Version version)
    {
        final String[] selfParts = version.self.split("/");
        final String versionId = selfParts[selfParts.length - 1];
        return putResponse(versionId, version);
    }

    /**
     * Returns a WebResponse for the version with the given id.
     *
     * @param versionID a String containing a version id
     * @return a WebTarget
     */
    private WebTarget versionWithID(String versionID)
    {
        return createResource().path("version").path(versionID);
    }

    /**
     * Returns a WebResponse for the version resource
     *
     * @return a WebTarget
     */
    private Invocation.Builder version()
    {
        return createResource().path("version").request(MediaType.APPLICATION_JSON);
    }

    /**
     * Returns a WebResponse for the version resource
     *
     * @return a WebTarget
     */
    private Invocation.Builder versionMove(String versionID)
    {
        return createResource().path("version").path(versionID).path("move").request(MediaType.APPLICATION_JSON);
    }
}
