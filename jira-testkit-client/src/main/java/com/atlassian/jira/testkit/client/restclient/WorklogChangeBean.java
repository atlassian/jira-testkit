package com.atlassian.jira.testkit.client.restclient;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class WorklogChangeBean
{
    private Long worklogId;
    private Long updatedTime;

    public WorklogChangeBean()
    {
    }

    public WorklogChangeBean(final Long worklogId, final Long updatedTime)
    {
        this.worklogId = worklogId;
        this.updatedTime = updatedTime;
    }

    public Long getWorklogId()
    {
        return worklogId;
    }

    public Long getUpdatedTime()
    {
        return updatedTime;
    }
}
