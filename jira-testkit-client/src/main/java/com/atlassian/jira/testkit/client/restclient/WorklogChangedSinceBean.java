package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class WorklogChangedSinceBean
{
    @JsonProperty
    public List<WorklogChangeBean> values;

    @JsonProperty
    public Long since;

    @JsonProperty
    public Long until;

    @JsonProperty
    public boolean lastPage;

    @JsonProperty
    public URI self;

    @JsonProperty
    public URI nextPage;
}
