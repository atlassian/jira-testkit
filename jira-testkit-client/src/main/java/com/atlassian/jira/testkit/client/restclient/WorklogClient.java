/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.restclient;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static javax.ws.rs.client.Entity.json;

/**
 * Client for the work log resource.
 *
 * @since v4.3
 */
public class WorklogClient extends RestApiClient<WorklogClient>
{
    /**
     * Constructs a new WorklogClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public WorklogClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * GETs the work log with the given id, returning a Response object.
     *
     * @param issueKey the issue key
     * @return a Response
     */
    public WorklogWithPaginationBean getAll(final String issueKey)
    {
        return createResource().path("issue").path(issueKey).path("worklog").request().get(WorklogWithPaginationBean.class);
    }

    /**
     * GETs the work log with the given id.
     *
     * @param issueKey the issue key
     * @param worklogID a String containing the work log id
     * @return a Worklog
     * @throws WebApplicationException if there is a problem getting the work log
     */
    public Worklog get(String issueKey, String worklogID) throws WebApplicationException
    {
        return worklogWithID(issueKey, worklogID).request().get(Worklog.class);
    }

    /**
     * GETs the work log with the given id, returning a Response object.
     *
     * @param issueKey the issue key
     * @param worklogID a String containing the work log id
     * @return a Response
     */
    public ParsedResponse getResponse(final String issueKey, final String worklogID)
    {
        return toResponse(() ->  worklogWithID(issueKey, worklogID).request().get(Response.class));
    }

    /**
     * Returns a WebTarget for the work log with the given id.
     *
     * @param issueKey worklog is associated with
     * @param worklogID a String containing the work log id
     * @return a WebTarget
     */
    protected WebTarget worklogWithID(String issueKey, String worklogID)
    {
        return createResource().path("issue").path(issueKey).path("worklog").path(worklogID);
    }

    public ParsedResponse<Worklog> put(final String issueKey, final Worklog worklog)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("worklog").path(worklog.id)
                        .request(MediaType.APPLICATION_JSON_TYPE).put(json(worklog), Response.class),
                Worklog.class);
    }

    public ParsedResponse<Worklog> put(final String issueKey, final Worklog worklog, final Map<String, String> queryParams)
    {
        return toResponse(() -> {
                WebTarget path = createResource().path("issue").path(issueKey).path("worklog").path(worklog.id);
                for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                    path = path.queryParam(entry.getKey(), entry.getValue());
                }
                return path.request(MediaType.APPLICATION_JSON_TYPE).put(json(worklog), Response.class);
            }, Worklog.class);
    }

    public ParsedResponse<Worklog> post(final String issueKey, final Worklog worklog)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("worklog").request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(worklog), Response.class), Worklog.class);
    }

    public ParsedResponse<Worklog> post(final String issueKey, final Worklog worklog, final Map<String, String> queryParams)
    {
        return toResponse(() -> {
            WebTarget path = createResource().path("issue").path(issueKey).path("worklog");
                    for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                       path = path.queryParam(entry.getKey(), entry.getValue());
                    }
                   return path.request(MediaType.APPLICATION_JSON_TYPE).post(json(worklog), Response.class);
            }, Worklog.class);
    }


    public ParsedResponse delete(final String issueKey, final Worklog worklog)
    {
        return toResponse(() ->  createResource().path("issue").path(issueKey).path("worklog").path(worklog.id)
                        .request(MediaType.APPLICATION_JSON_TYPE).delete(Response.class));
    }

    public ParsedResponse delete(final String issueKey, final Worklog worklog, final Map<String, String> queryParams) {
        return toResponse(() -> {
                WebTarget path = createResource().path("issue").path(issueKey).path("worklog").path(worklog.id);
                for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                    path = path.queryParam(entry.getKey(), entry.getValue());
                }
                return path.request(MediaType.APPLICATION_JSON_TYPE).delete(Response.class);
            });
    }

    public ParsedResponse<WorklogChangedSinceBean> getUpdatedWorklogsSince(Long since)
    {
        return toResponse(() ->  WorklogClient.this.createResource().path("worklog").path("updated")
                        .queryParam("since", String.valueOf(since)).request().get(Response.class),
        WorklogChangedSinceBean.class);
    }

    public ParsedResponse<WorklogChangedSinceBean> getDeletedWorklogsSince(Long since)
    {
        return toResponse(() ->  WorklogClient.this.createResource().path("worklog").path("deleted")
                        .queryParam("since", String.valueOf(since)).request().get(Response.class),
        WorklogChangedSinceBean.class);
    }

    public ParsedResponse<List<Worklog>> getWorklogs(Collection<Long> ids)
    {
        return toResponse(() ->  createResource().path("worklog").path("list")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(json(new WorklogIdsRequestBean(ids)), Response.class),
                new GenericType<List<Worklog>>() {});
    }

    public class WorklogIdsRequestBean
    {
        private Collection<Long> ids;

        public WorklogIdsRequestBean() { }

        public WorklogIdsRequestBean(Collection<Long> ids) { this.ids = ids; }

        public Collection<Long> getIds()
        {
            return ids;
        }

        public void setIds(final Set<Long> ids)
        {
            this.ids = ids;
        }
    }
}
