/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.client.util;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.1
 */
public class TimeBombLicence {

	public static final String LICENCE_FOR_TESTING =
			"AAACeg0ODAoPeNp1VNFymkAUfecrdqYvbWYwgG2TOMODAommEQxiTFP7sC4X2QR3ye6itV/fFWUin\n" +
			"eTR473n3nPOXT4lFaCIKGRbyLrqWRe9bhfdjBPkWE7XmILYgBj57uDmKjEfZw9fzR9PT0NzYNlz4\n" +
			"44SYBKSXQkhXoM7TfpxEsQGEXybdk7+db09sughHzZQ8BKEQTjLOpgougFXiQp0U0XosoAWeOQI/\n" +
			"pRU7HyswJ3YjjV8K24POYCtOc9U4BZlWK2XIKJsJkFI17YMHyQRtFSUMzcBqVBx4EQZF6gsqhVlK\n" +
			"G34JPp8qxmR730xjnUPGt33OkawwUWFa6IMFxKMMaZMAcOMvCNhb0BrfQ0UFejalgBPQE1Ztzq27\n" +
			"ZiWYzrfDv2NGK+opAIR8hSkaxlnZ2deHPSTwDcHP/UPF68Yxelf+mKQgutAW468wK7RYH+3rAvrs\n" +
			"tu1jSVeLzlvbTioIRRoTaIUVLYXbTcEKa2NCEJ9EZN4NA2a0S3NNdTWy5nS2wXau+J07/qmTteuk\n" +
			"/142jGdIZa5O/a23vXN/D57SC4ZDL7N2bb0B+vu3XBqpZi+9vP7BOeZHVnYJrfh62x794hhXJC5Y\n" +
			"z/2F+7CNTIqc9i1r1M7G0aJeR3F5iSO/JmXjKLQnE2DveV1bpCi5Q6pHNBxUW0e0SHpwxL8GfSr+\n" +
			"5UrVS565+cr3sGqwFJSzDqEr8+PAkw4dPzuIJ8jxhVKqVSCLisFmplKpDgiOn6+1iF2jEisMKPyc\n" +
			"Ib9hrF5Sfolz9gL41vW5HUqaFIJkmMJ/9/aqdV1brejuP/RHdTFmgF7sC84MDf2nZJcayzYwXvp1\n" +
			"5+Tt/D/AWRvnywwLAIUEZXThN7ewKC0ZpUJNMb0AjCOFOoCFEc6FJjjyL/r53YmAwBO/uYEuSROX\n" +
			"02td";

	public static final String V2_PERSONAL =
			"AAAB6w0ODAoPeNplklmP2jAUhd/9Kyz1jSqQhZaClIeQZIB2SNIs0JnSByfcAQ9gZ2yH7dfXw6JWr\n" +
			"WQpkm/O8Tmf/SFvAMeVwpaJzf5AL8vCo2mObdN20AZOMxCScuZan02zZ35xHAtFza4EEb8UUs9cB\n" +
			"/mcKVKpiOzAJStGyfJMNyhpRLUmEgKiwLVNq2uYPcNy0COtgEnITzVcFEmYZnHkPd4H4bGm4nRRJ\n" +
			"ZZtju/24ZTQ7V/+GYg9iEngDkf93PhRzLrGt+fnsTE0rTnaXr3GRK7dqW/6D0PbMV/IRznszj6Fv\n" +
			"SA/lk8F37Nd+QbeuvBW1Tk/jLKjPS873ncIOmXfOfdn84O3WrjolQrSvsdbUvXOI4zyME3SSRaiV\n" +
			"qsVxbnxEKdGksZB4eeTODKKLNQD1xegqyxxecJqDfjmgkNW8SUIXAv+Chr/z7VS9WLQ6ax4m6gtk\n" +
			"ZIS1q74rnOrYsBV8auNA44ZV3hJpRK0bBRoZyqx4rhqpOI7fSttpGkxBYyw6n+iOpefhl4eBsbw6\n" +
			"T3kH6q3fBprwTaMH9i1vBYTH7ShcJVoAGVh5OYglf6iWKwIo5JcsHj37OhSXG/9e/+3Pvd3ZaMAZ\n" +
			"CVofZF/naQeDnyNRx9VCyphMcCJ/pUzsr1G0U+B7uEa4zd29un3MCwCFBicfopnlHWoWEWnxfLGN\n" +
			"W0kKbbRAhRlOVk8r6dKO+p0/WDeNxf+/IjP8Q==X02nb";
}
