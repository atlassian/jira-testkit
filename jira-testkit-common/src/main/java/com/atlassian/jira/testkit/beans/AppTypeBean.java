package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JAXB bean for AppLinks ApplicationType.
 *
 * @since v4.3
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppTypeBean
{
    @JsonProperty
    private String i18nKey;

    @JsonProperty
    private String iconUrl;

    public AppTypeBean()
    {
    }

    public AppTypeBean(String i18nKey, String iconUrl)
    {
        this.i18nKey = i18nKey;
        this.iconUrl = iconUrl;
    }
}
