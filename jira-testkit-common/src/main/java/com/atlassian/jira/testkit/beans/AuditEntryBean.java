package com.atlassian.jira.testkit.beans;

import com.atlassian.jira.user.ApplicationUser;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 *
 * @since 7.0.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(NON_NULL)
public class AuditEntryBean
{
    @JsonProperty public String category;
    @JsonProperty public String summaryI18nKey;
    @JsonProperty public String eventSource;
    @JsonProperty public ApplicationUser author;
    @JsonProperty public String remoteAddress;
    @JsonProperty public String description;
}
