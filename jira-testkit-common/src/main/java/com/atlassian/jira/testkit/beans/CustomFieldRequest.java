package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = ANY)
public class CustomFieldRequest
{
    public String name;
    public String description;
    public String type;
    public String searcherKey;
    public UserFilter userFilter;
}

