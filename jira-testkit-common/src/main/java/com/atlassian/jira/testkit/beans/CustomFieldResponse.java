package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

@JsonIgnoreProperties (ignoreUnknown = true)
public class CustomFieldResponse implements Named
{
    @JsonProperty
    public String name;

    @JsonProperty
    public String id;

    @JsonProperty
    public String type;

    @JsonProperty
    public String description;

    @JsonProperty
    public String searcher;

    @JsonProperty
    public String untranslatedName;

    @JsonProperty
    public String untranslatedDescription;

    //Don't set this to an empty list by default or old clients wont be compatible with the GET call.
    private List<CustomFieldConfig> config;

    public CustomFieldResponse(final String name, final String id, final String type, final String description, final String searcher, final String untranslatedName, final String untranslatedDescription)
    {
        this.description = description;
        this.id = id;
        this.name = name;
        this.searcher = searcher;
        this.type = type;
        this.untranslatedName = untranslatedName;
        this.untranslatedDescription = untranslatedDescription;
    }

    public CustomFieldResponse(final String name, final String id, final String type, final String description, final String searcher) {
        this(name, id, type, description, searcher, null, null);
    }

    public CustomFieldResponse(String name, String id, String type)
    {
        this(name, id, type, null, null);
    }

    public CustomFieldResponse()
    {
    }

    @Override
    public boolean equals(final Object o)
    {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String getName()
    {
        return name;
    }

    @JsonProperty
    public List<CustomFieldConfig> getConfig()
    {
        return config;
    }

    public void setConfig(final List<CustomFieldConfig> config)
    {
        this.config = config;
    }
}