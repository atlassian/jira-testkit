package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * JAXB-enabled entity list.
 *
 * @since v4.3
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class EntityList
{
    public ArrayList<EntityRefBean> entities;

    public EntityList()
    {
    }

    public EntityList(ArrayList<EntityRefBean> entities)
    {
        this.entities = entities;
    }
}
