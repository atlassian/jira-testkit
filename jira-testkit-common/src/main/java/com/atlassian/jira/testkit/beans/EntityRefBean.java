package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * JAXB bean for AppLinks EntityReference.
 *
 * @since v4.3
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class EntityRefBean
{
    public String key;
    public EntityTypeBean type;
    public String name;

    public EntityRefBean()
    {
    }

    public EntityRefBean(String key, EntityTypeBean type, String name)
    {
        this.key = key;
        this.type = type;
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "EntityRefBean{" +
                "key='" + key + '\'' +
                ", type=" + type +
                ", name='" + name + '\'' +
                '}';
    }
}
