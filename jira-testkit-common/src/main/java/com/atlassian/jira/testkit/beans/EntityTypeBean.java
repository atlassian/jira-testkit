package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * JAXB bean for AppLinks EntityType.
 *
 * @since v4.3
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class EntityTypeBean
{
    public String applicationTypeClassName;
    public String i18nKey;
    public String pluralizedI18nKey;
    public String iconUrl;

    public EntityTypeBean()
    {
    }

    public EntityTypeBean(String applicationTypeClassName, String i18nKey, String pluralizedI18nKey, String iconUrl)
    {
        this.applicationTypeClassName = applicationTypeClassName;
        this.i18nKey = i18nKey;
        this.pluralizedI18nKey = pluralizedI18nKey;
        this.iconUrl = iconUrl;
    }

    @Override
    public String toString()
    {
        return "EntityTypeBean{" +
                "applicationTypeClassName='" + applicationTypeClassName + '\'' +
                ", i18nKey='" + i18nKey + '\'' +
                ", pluralizedI18nKey='" + pluralizedI18nKey + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                '}';
    }
}
