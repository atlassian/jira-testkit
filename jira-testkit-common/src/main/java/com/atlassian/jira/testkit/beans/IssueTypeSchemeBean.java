package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JSON representation of an issue type scheme. The current state is not exhaustive of the scheme's state - add
 * attributes as you need them here.
 *
 * @since 6.3
 */
@JsonIgnoreProperties (ignoreUnknown = true)
public class IssueTypeSchemeBean
{
    @JsonProperty
    public Long id;
    @JsonProperty
    public String name;

    public IssueTypeSchemeBean()
    {
    }

    public IssueTypeSchemeBean(final Long id, final String name)
    {
        this.id = id;
        this.name = name;
    }
}
