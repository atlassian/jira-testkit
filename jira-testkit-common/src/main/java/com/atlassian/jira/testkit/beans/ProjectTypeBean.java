package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties (ignoreUnknown = true)
public class ProjectTypeBean
{

    @JsonProperty
    private String key;
    @JsonProperty
    private String descriptionI18nKey;
    @JsonProperty
    private String icon;
    @JsonProperty
    private String color;

    public String getKey()
    {
        return key;
    }

    public String getDescriptionI18nKey()
    {
        return descriptionI18nKey;
    }

    public String getIcon()
    {
        return icon;
    }

    public String getColor()
    {
        return color;
    }
}
