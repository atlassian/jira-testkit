package com.atlassian.jira.testkit.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UserFilter {

    public boolean enabled;
    public Set<String> groups;
    public Set<Long> roleIds;

    public UserFilter(){}

    public UserFilter(final boolean enabled, final Set<String> groups, final Set<Long> roleIds) {
        this.enabled = enabled;
        this.groups = groups;
        this.roleIds = roleIds;
    }
}
