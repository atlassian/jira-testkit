package com.atlassian.jira.testkit.client.restclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectRoleActorsBean
{
    private Collection<ProjectRole.Actor> actors;

    public ProjectRoleActorsBean() {}

    public ProjectRoleActorsBean(final Collection<ProjectRole.Actor> actors)
    {
        this.actors = actors;
    }

    @JsonProperty
    public Collection<ProjectRole.Actor> getActors()
    {
        return actors;
    }

}
