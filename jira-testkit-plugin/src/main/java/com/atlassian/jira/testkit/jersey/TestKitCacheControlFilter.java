package com.atlassian.jira.testkit.jersey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class TestKitCacheControlFilter implements ContainerResponseFilter {
    private static final Logger log = LoggerFactory.getLogger(TestKitCacheControlFilter.class);

    public TestKitCacheControlFilter() {
        // empty
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {

        MultivaluedMap<String, Object> headers = containerResponseContext.getHeaders();
        if (!headers.containsKey("Cache-Control") && !headers.containsKey("Expires"))
        {
            log.trace("Response does not have caching headers, adding 'Cache-Control: no-cache, no-store'");
            headers.putSingle("Cache-Control", "no-cache, no-store");
        }
    }
}
