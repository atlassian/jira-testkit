/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.plugin;

import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A backdoor for indexing
 *
 * @since v5.1
 */
@Path("indexing")
@Produces ({MediaType.APPLICATION_JSON})
@Consumes ({MediaType.APPLICATION_JSON})
public class IndexingBackdoor
{
    private final IssueIndexingService issueIndexingService;

    @Inject
    public IndexingBackdoor(IssueIndexingService issueIndexingService)
    {
        this.issueIndexingService = issueIndexingService;
    }

    @POST
    @UnrestrictedAccess
    @Path("reindexAll")
    public Response reindexAll()
    {
        try {
            issueIndexingService.reIndexAll();
        } catch (IndexException e) {
            throw new RuntimeException(e);
        }
        return Response.ok().build();
    }
}