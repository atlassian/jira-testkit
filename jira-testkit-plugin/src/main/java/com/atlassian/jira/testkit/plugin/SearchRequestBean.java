/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.plugin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
* Stores state for a {@link com.atlassian.jira.issue.search.SearchRequest} representation being passed via REST.
*
* @since v5.0
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchRequestBean
{
    @JsonProperty
    public String username;
    @JsonProperty
    public String searchJql;
    @JsonProperty
    public String searchName;
    @JsonProperty
    public String searchDescription;
    @JsonProperty
    public String jsonShareString;
    @JsonProperty
    public boolean favourite;
    @JsonProperty
    public Long favouriteCount;
}
