/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.plugin;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.crowd.exception.runtime.UserNotFoundException;
import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.testkit.beans.DirectoryDTO;
import com.atlassian.jira.testkit.beans.LoginInfoBean;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status;

/**
 * Use this backdoor to manipulate Users and Groups as part of setup for tests
 * NOT specifically testing the Admin UI.
 * <p>
 * This class should only be called by the com.atlassian.jira.testkit.client.UsersAndGroupsControl in jira-testkit-client.
 *
 * @since v5.0
 */
@Path("usersAndGroups")
public class UsersAndGroupsBackdoor {
    private static final Logger log = LoggerFactory.getLogger(UsersAndGroupsBackdoor.class);
    private static final Response NOT_FOUND = Response.status(Status.NOT_FOUND).build();
    private static final Response OK = Response.ok().build();
    private static final String EMPTY_QUERY = "";
    private static final UserSearchParams ALL_USERS_SEARCH_PARAMS = UserSearchParams.builder()
            .allowEmptyQuery(true)
            .includeActive(true)
            .includeInactive(true)
            .build();

    private final CrowdService crowdService;
    private final LoginService loginService;
    private final UserUtil userUtil;
    private final UserManager userManager;
    private final GroupPickerSearchService groupPickerSearchService;
    private final UserSearchService userSearchService;
    private final CrowdDirectoryService crowdDirectoryService;
    private final UserService userService;

    @Inject
    public UsersAndGroupsBackdoor(final UserUtil userUtil,
                                  final CrowdService crowdService,
                                  final LoginService loginService,
                                  final UserManager userManager,
                                  final GroupPickerSearchService groupPickerSearchService,
                                  final UserSearchService userSearchService,
                                  final CrowdDirectoryService crowdDirectoryService,
                                  final UserService userService) {
        this.crowdService = crowdService;
        this.userUtil = userUtil;
        this.loginService = loginService;
        this.userManager = userManager;
        this.groupPickerSearchService = groupPickerSearchService;
        this.userSearchService = userSearchService;
        this.crowdDirectoryService = crowdDirectoryService;
        this.userService = userService;
    }

    @GET
    @UnrestrictedAccess
    @Path("user/addEvenIfUserExists")
    public Response addEvenIfuserExists(
            @QueryParam("userName") final String username,
            @QueryParam("password") final String password,
            @QueryParam("email") final String email,
            @QueryParam("displayName") final String displayName,
            @QueryParam("sendEmail") final boolean sendEmail) {
        if (userUtil.userExists(username)) {
            deleteUser(username);
        }
        return addUser(username, password, email, displayName, sendEmail);
    }

    @GET
    @UnrestrictedAccess
    @Path("user/add")
    public Response addUser(
            @QueryParam("userName") final String username,
            @QueryParam("password") final String password,
            @QueryParam("email") final String email,
            @QueryParam("displayName") final String displayName,
            @QueryParam("sendEmail") final boolean sendEmail) {
        try {
            doAddUser(username, password, email, displayName, sendEmail);
        } catch (PermissionException e) {
            log.warn("PermissionException adding user", e);
            throw new RuntimeException(e);
        } catch (CreateException e) {
            log.warn("CreateException adding user", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    private void doAddUser(final String username, final String password, final String email, final String displayName, final boolean sendEmail)
            throws PermissionException, CreateException {
        final UserService.CreateUserRequest createUserRequest = UserService.CreateUserRequest
                .withUserDetails(null, username, password, email, displayName)
                .sendNotification(sendEmail)
                .skipValidation();
        userService.createUser(userService.validateCreateUser(createUserRequest));
    }

    @GET
    @UnrestrictedAccess
    @Path("user/delete")
    public Response deleteUser(@QueryParam("userName") final String username) {
        final ApplicationUser admin = userManager.getUserByName("admin");     // shouldn't have to pass admin in for permissions at this level...
        final ApplicationUser userToRemove = userManager.getUserByName(username);

        userUtil.removeUser(admin, userToRemove);

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/addToGroup")
    public Response addUserToGroup(
            @QueryParam("userName") final String userName,
            @QueryParam("groupName") final String groupName) {
        final Group group = crowdService.getGroup(groupName);
        final ApplicationUser userToAdd = userManager.getUserByName(userName);

        try {
            userUtil.addUserToGroup(group, userToAdd);
        } catch (PermissionException e) {
            log.warn("PermissionException adding user to group", e);
            throw new RuntimeException(e);
        } catch (AddException e) {
            log.warn("PermissionException adding user to group", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/removeFromGroup")
    public Response removeUserFromGroup(
            @QueryParam("userName") final String userName,
            @QueryParam("groupName") final String groupName) {
        final Group group = crowdService.getGroup(groupName);
        final ApplicationUser userToRemove = userManager.getUserByName(userName);

        try {
            userUtil.removeUserFromGroup(group, userToRemove);
        } catch (PermissionException e) {
            log.warn("PermissionException removing user from group", e);
            throw new RuntimeException(e);
        } catch (RemoveException e) {
            log.warn("RemoveExceptionNotPermittedException removing user from group", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/add")
    public Response addGroup(@QueryParam("groupName") final String groupName) {
        try {
            crowdService.addGroup(new GroupTemplate(groupName));
        } catch (InvalidGroupException e) {
            log.warn("InvalidGroupException adding group", e);
            throw new RuntimeException(e);
        } catch (OperationNotPermittedException e) {
            log.warn("OperationNotPermittedException adding group", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/exists")
    public Response groupExists(@QueryParam("groupName") final String groupName) {
        final boolean exists = crowdService.getGroup(groupName) != null;
        return Response.ok(Boolean.toString(exists)).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/delete")
    public Response deleteGroup(@QueryParam("groupName") final String groupName) {
        try {
            crowdService.removeGroup(new GroupTemplate(groupName));
        } catch (OperationNotPermittedException e) {
            log.warn("OperationNotPermittedException adding group", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/addToGroup")
    public Response addGroupToGroup(
            @QueryParam("groupName") final String groupName,
            @QueryParam("parentGroupName") final String parentGroupName) {
        final Group childGroup = crowdService.getGroup(groupName);
        final Group parentGroup = crowdService.getGroup(parentGroupName);

        try {
            crowdService.addGroupToGroup(childGroup, parentGroup);
        } catch (OperationNotPermittedException | InvalidMembershipException e) {
            log.warn("Exception adding group to group", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/addMany")
    public Response addManyGroups(
            @QueryParam("groupNamePrefix") final String groupNamePrefix,
            @QueryParam("numberOfNewGroups") final int numberOfNewGroups,
            @Nullable @QueryParam("parentGroupName") final String parentGroupName,
            @Nullable @QueryParam("numberOfNewUsersPerGroup") final Integer numberOfNewUsers) {
        Group parentGroup = null;
        if (StringUtils.isNotBlank(parentGroupName)) {
            parentGroup = crowdService.getGroup(parentGroupName);
        }
        try {
            for (int i = 0; i < numberOfNewGroups; i++) {
                final String groupName = groupNamePrefix + i;
                final Group newGroup = crowdService.addGroup(new GroupTemplate(groupName));
                if (parentGroup != null) {
                    crowdService.addGroupToGroup(newGroup, parentGroup);
                }

                if (numberOfNewUsers != null && numberOfNewUsers > 0) {
                    final String prefix = groupName + "-user";
                    addManyUsers(prefix, prefix, numberOfNewUsers, newGroup.getName());
                }
            }
        } catch (InvalidGroupException | OperationNotPermittedException | InvalidMembershipException e) {
            log.warn("Exception adding groups", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/addMany")
    public Response addManyUsers(
            @QueryParam("usernamePrefix") final String usernamePrefix,
            @QueryParam("displayNamePrefix") final String displayNamePrefix,
            @QueryParam("numberOfNewUsers") final int numberOfNewUsers,
            @javax.annotation.Nullable @QueryParam("groupName") final String groupName) {
        Group group = null;
        if (StringUtils.isNotBlank(groupName)) {
            group = crowdService.getGroup(groupName);
        }
        try {
            for (int i = 0; i < numberOfNewUsers; i++) {
                final String username = usernamePrefix + i;
                doAddUser(username,
                        username,
                        "e" + username + "@example.com",     // make the email slightly different to the username
                        displayNamePrefix + i, false);

                if (group != null) {
                    final ApplicationUser user = userManager.getUserByName(username);
                    userUtil.addUserToGroup(group, user);
                }
            }
        } catch (PermissionException e) {
            log.warn("Permission exception adding users", e);
            throw new RuntimeException(e);
        } catch (CreateException e) {
            log.warn("Create exception adding users", e);
            throw new RuntimeException(e);
        } catch (AddException e) {
            log.warn("Add exception adding users", e);
            throw new RuntimeException(e);
        }

        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/resetLoginCount")
    public Response resetLoginCount(@QueryParam("user") final String username) {
        final ApplicationUser user = userManager.getUserByName(username);
        loginService.resetFailedLoginCount(user);
        return Response.ok(null).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/count")
    public Response numberOfUsers() {
        final String count = Long.toString(userManager.getTotalUserCount());
        return Response.ok(count).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/count")
    public Response numberOfGroups() {
        final int count = groupPickerSearchService.findGroups(EMPTY_QUERY).size();
        final String stringCount = Long.toString(count);
        return Response.ok(stringCount).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/includes")
    @Produces({MediaType.APPLICATION_JSON})
    public Response groupIncludes(@QueryParam("groupName") final String groupName, @QueryParam("userName") final String userName) {
        return Response.ok(userUtil.getGroupNamesForUser(userName).contains(groupName)).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("group/includesGroup")
    @Produces({MediaType.APPLICATION_JSON})
    public Response groupIncludesGroup(
            @QueryParam("groupName") final String groupName,
            @QueryParam("parentGroupName") final String parentGroupName) {
        final Group childGroup = crowdService.getGroup(groupName);
        final Group parentGroup = crowdService.getGroup(parentGroupName);

        return Response.ok(crowdService.isGroupMemberOfGroup(childGroup, parentGroup)).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/exists")
    @Produces({MediaType.APPLICATION_JSON})
    public Response userExists(@QueryParam("userName") final String userName) {
        return Response.ok(userUtil.userExists(userName)).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/loginInfo")
    @Produces({MediaType.APPLICATION_JSON})
    public Response loginInfo(@QueryParam("userName") final String userName) {
        final LoginInfo info = loginService.getLoginInfo(userName);
        if (info == null) {
            return Response.status(Status.BAD_REQUEST).entity("No user '" + userName + "' found").build();
        }
        final LoginInfoBean bean = new LoginInfoBean();
        bean.setLoginCount(nullToZero(info.getLoginCount()));
        bean.setCurrentFailedLoginCount(nullToZero(info.getCurrentFailedLoginCount()));
        bean.setTotalFailedLoginCount(nullToZero(info.getTotalFailedLoginCount()));
        return Response.ok(bean).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/byName")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserByName(@QueryParam("userName") final String userName) {
        final ApplicationUser user = userManager.getUserByName(userName);
        if (user == null) {
            return NOT_FOUND;
        }
        return Response.ok(new UserDTO(user)).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/byNameEvenWhenUnknown")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserByNameEvenWhenUnknown(@QueryParam("userName") final String userName) {
        final ApplicationUser user = userManager.getUserByNameEvenWhenUnknown(userName);
        if (user == null) {
            return Response.status(Status.BAD_REQUEST).entity("Passed username was null").build();
        }
        return Response.ok(new UserDTO(user)).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("user/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers() {
        return Response.ok(Collections2.transform(userSearchService.findUsers(EMPTY_QUERY, ALL_USERS_SEARCH_PARAMS), user -> new UserDTO(user))).build();
    }

    @POST
    @UnrestrictedAccess
    @Path("user/byName")
    @XsrfProtectionExcluded // Only available during testing.
    public Response updateUser(final UserDTO user) {
        if (log.isDebugEnabled()) {
            log.debug("Updating user with: " + user);
        }
        final ApplicationUser existingUser = userManager.getUserByKey(user.getKey());
        if (existingUser == null) {
            return NOT_FOUND;
        }
        final ApplicationUser updatedUser = user.asApplicationUser(existingUser.getDirectoryUser());
        try {
            userManager.updateUser(updatedUser);
        } catch (UserNotFoundException e) {
            return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
        }
        return OK;
    }

    @GET
    @UnrestrictedAccess
    @Path("directory")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDirectories() {
        return Response.ok(Lists.transform(crowdDirectoryService.findAllDirectories(),
                directory -> new DirectoryDTO(directory))).build();
    }

    @GET
    @UnrestrictedAccess
    @Path("directory/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDirectory(@PathParam("id") final long id) {
        return Response.ok(new DirectoryDTO(crowdDirectoryService.findDirectoryById(id))).build();
    }

    private static long nullToZero(final Long theLong) {
        return theLong != null ? theLong : 0;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class GroupTemplate implements Group {
        @JsonProperty
        private final String groupName;

        public GroupTemplate(final String groupName) {
            this.groupName = groupName;
        }

        @Override
        public String getName() {
            return groupName;
        }

        @Override
        public int compareTo(final Group o) {
            return groupName.compareTo(o.getName());
        }
    }

}
